﻿<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Vestibular no Montese | Ingresse agora mesmo na Faculdade Ateneu Montese! Ingresse tamb&#233;m com sua nota do ENEM. Saiba mais.">
    <meta name="keywords" content="montese, vestibular, extra montese, efivest montese, cursinho montese, faculdade montese, piamarta montese, idec montese, uva montese">
    <meta name="author" content="Faculdade Ateneu">
    <meta name="robots" content="noindex" />
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">
    <link rel="canonical" href="http://queroserateneu.com.br/montese">

    <title>Seja Bem-vindo à Faculdade Ateneu Montese</title>

    <!-- CSS -->
    <link href="assets/css/main.min.css" rel="stylesheet"> 
  </head>
  <body>
    <!-- MAIN -->
    <div class="container">
      <div class="table">
        <div class="table-cell">
          <!-- Logo -->
          <div class="col-md-12 logo">
            <picture>
              <img src="assets/images/logo-faculdade-ateneu.png" alt="Faculdade Ateneu | Criando Valores">
              <br>
              <br>
              <br>
            </picture>
          </div>

          <!-- Destaque -->
          <div class="col-md-12 col-sm-12 col-xs-12 destaque">
            <div class="col-md-5 col-sm-6 hidden-xs imagem">
              <picture>
                <img src="assets/images/vestibular-faculdade-ateneu-cursos-graduacao.jpg" alt="Vestibular em Fortaleza | Faculdade Ateneu">
              </picture>
            </div>

            <div class="col-md-7 col-sm-6 col-xs-12 texto">
            <br class="visible-lg">
            <br class="visible-lg">
            <br class="visible-lg visible-md">
            <br class="visible-lg visible-md">  
                <h2 class="blue-color">Parab&#233;ns pela sua inscri&#231;&#227;o!</h2>
                <h1>
                  SEJA BEM-VINDO &#192; <br>FACULDADE ATENEU
                </h1>
                <p class="hidden-xs hidden-sm">Acesse o seu e-mail e siga as nossas instru&#231;&#245;es para obter a<br>
                  <span class="blue-color">oferta especial</span> que preparamos para voc&#234;. Obrigado por fazer sua<br>
                  pr&#233;-inscri&#231;&#227;o na Faculdade Ateneu. Acesse o seu e-mail agora!<br><br>
                  <a href="https://mail.google.com/" target="_blank"><img src="assets/images/gmail-icon.png" alt="Acessar Gmail"></a>
                  <a href="https://login.live.com/" target="_blank"><img src="assets/images/outlook-icon.png" alt="Acessar Outlook"></a>
                  <a href="http://br.mail.yahoo.com/" target="_blank"><img src="assets/images/yahoo-icon.png" alt="Acessar Yahoo Mail"></a>
                </p>
                <p class="visible-xs visible-sm">Acesse o seu e-mail e siga as nossas instru&#231;&#245;es para obter a oferta especial que preparamos para voc&#234;. Obrigado por fazer suas
                  pr&#233;-inscri&#231;&#227;o em nosso vestibular. Acesse o seu e-mail agora!<br><br>
                  <a href="https://mail.google.com/" target="_blank"><img src="assets/images/gmail-icon.png" alt="Acessar Gmail"></a>
                  <a href="https://login.live.com/" target="_blank"><img src="assets/images/outlook-icon.png" alt="Acessar Outlook"></a>
                  <a href="http://br.mail.yahoo.com/" target="_blank"><img src="assets/images/yahoo-icon.png" alt="Acessar Yahoo Mail"></a>
                </p>
            </div>
          </div>
          <!-- Close Destaque -->
         
        </div><!-- /END Table-Cell -->
      </div><!-- /END Table -->
    </div><!-- /END Container -->    

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.validate.js"></script>
    <script type="text/javascript" src="assets/js/jquery.mask.js"></script>
    <script type="text/javascript" src="assets/js/functions.js"></script>
  </body>
</html>
