<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Black Friday Cursos Tecnicos | 90% na Matrícula Cursos Tecnicos</title>
    <meta name="description" content="Black Friday Cursos Tecnicos. Mega Desconto de 90% na Matrícula em 8 Cursos Tecnicos na Escola Politécnica Ateneu. Inscreva-se Agora no Black Friday!">
    <meta name="keywords" content="cursos tecnicos, black friday, mega oferta, cursos livres, black november, 90% desconto, escola politénica ateneu, faculdade ateneu, oferta especial, carreira.">
    <meta name="author" content="Faculdade Ateneu">
    <meta name="robots" content="noodp,noydir">
    <link rel="shortcut icon" href="images/faviconblack.ico" type="image/x-icon">
    <link rel="icon" href="images/faviconblack.ico" type="image/x-icon">
    <link rel="canonical" href="http://queroserateneu.com.br/black-friday-cursos-tecnicos/">

    <meta property="og:locale" content="pt_BR">
    <meta property="og:type" content="university">
    <meta property="og:title" content="Black Friday | Cursos Técnicos Ateneu">
    <meta property="og:description" content="São 8 Cursos Técnicos com Mega Desconto de 90% na Matrícula + 25% nas 5 Primeiras Mensalidades.">
    <meta property="og:url" content="http://queroserateneu.com.br/black-friday-cursos-tecnicos/">
    <meta property="og:site_name" content="Black Friday | Cursos Técnicos Ateneu">
    <meta property="og:image" content="http://queroserateneu.com.br/black-friday-cursos-tecnicos/images/cursos-tecnicos-ateneu-black-friday-facebook.jpg">

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">
  </head>

  <body>
    <!-- MAIN -->
    <div class="container">
      <div class="table">
        <div class="table-cell">
          <!-- Logo -->
          <div class="col-md-12 col-sm-12 col-xs-12 logo">
            <picture>
              <img src="images/logo-faculdade-ateneu-montese-white.png" alt="Escola Politecnica Ateneu | 8 Cursos Técnicos em Fortaleza">
            </picture>
          </div>

          <!-- Destaque -->
          <div class="col-md-12 col-sm-12 col-xs-12 banner">
              <picture>
                <img src="images/black-friday-cursos-tecnicos-ateneu.jpg" alt="Black Friday Cursos Técnicos Ateneu" class="img-responsive">
              </picture>
              <div class="col-md-12 col-sm-12 col-xs-12 line"></div>
              <h1 class="col-md-7 col-sm-12 col-xs-12 textobanner">
                <span class="yellow-color">INSCRIÇÕES ENCERRADAS!</span><br>
                A Escola Politécncia Ateneu agradece a sua participação no <span class="yellow-color">Black Friday</span> e o parabeniza
                por ter aproveitado a oportunidade de começar 2016 dando um salto na carreira. <span class="yellow-color">Te desejamos todo o sucesso!</span>
              </h1>
              <div class="col-md-5 col-sm-12 col-xs-12 relogio">
                
                <div class="col-md-12 contador">
                  <div class="col-md-3 col-sm-3 col-xs-3 bloco">
                    <p class="legenda">Dias</p>
                    <div class="numero" id="dia">0</div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-3 bloco">
                    <p class="legenda">Horas</p>
                    <div class="numero" id="hora">0</div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-3 bloco">
                    <p class="legenda">Min</p>
                    <div class="numero" id="minuto">0</div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-3 bloco">
                    <p class="legenda">Seg</p>
                    <div class="numero" id="segundo">0</div>
                  </div>
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12 line"></div>
              <h2 class="col-md-12 col-sm-12 col-xs-12" style="text-align:center;">
                <span class="yellow-color">GOSTOU DO BLACK FRIDAY CURSOS TÉCNICOS ATENEU?</span><br>
                PREENCHA SEUS DADOS E RECEBA EM PRIMEIRÍSSIMA MÃO TODAS AS NOVIDADES E PROMOÇÕES DOS NOSSOS CURSOS TÉCNICOS DIRETAMENTE NO SEU E-MAIL.
              </h2>
          </div>
          <!-- Close Destaque -->

          <!-- Open Formulário -->
          <div class="col-md-12 col-sm-12 col-xs-12 formulario">
            <form method="post" id="easyform_4e6eDxue1gOcSBXqQe6b-74c26" class="easyform_4e6eDxue1gOcSBXqQe6b-74c26" enctype="multipart/form-data" action="http://mkt.queroserateneu.com.br/w/4e6eDxue1gOcSBXqQe6b-74c26">
              <input type="hidden" name="lista" value="4">
              <input type="hidden" name="cliente" value="138749">
              <input type="hidden" name="lang" id="lang_id" value="br">
              <input type="hidden" name="formid" id="formid" value="6">

              <?php
              $string = basename($_SERVER['QUERY_STRING']);
              $dados = explode('&',$string);
              $total = count($dados);
              $array = '';
              echo '<input type="text" name="campoe_13_53" alt="" id="campoe_13_53" easylabel="Campanha" value="'.$dados[0].'" easysync="campoe_13" style="display:none;">';
              echo '<input type="text" name="campoe_14_54" alt="" id="campoe_14_54" easylabel="Origem" value="'.$dados[1].'" easysync="campoe_14" style="display:none;">';
              ?>
      
              <!-- Inputs -->
              <div class="col-md-12 col-sm-12 col-xs-12 fields">
                <div class="form-group col-md-3">
                  <input type="text" name="fname_34" easylabel="Nome" alt="" id="fname_34" class="form-control" value="" easysync="fname" placeholder="Nome">
                </div>
                <div class="form-group col-md-3">
                  <input type="email" name="email_35" easylabel="E-mail" alt="" id="email_35" class="form-control" value="" easysync="email" easyvalidation="true" placeholder="E-mail">
                </div>
                <div class="form-group col-md-3">
                  <select class="form-control" data-theme="grey" name="campoe_12_50" id="campoe_12" easysync="campoe_12" easylabel="Escolha o seu Curso" useoutrolabel="Outro (qual?)">
                    <option style="display: block;" value="10" title="Escolha o seu Curso:" order="8" visible="visible" selected="selected">
                      Escolha o seu Curso:
                    </option>
                    <option style="" value="1" title="T&#233;cnico em Administra&#231;&#227;o" order="0" visible="visible">
                      T&#233;cnico em Administra&#231;&#227;o
                    </option>
                    <option style="" value="2" title="T&#233;cnico em Edifica&#231;&#245;es" order="1" visible="visible">
                      T&#233;cnico em Edifica&#231;&#245;es
                    </option>
                    <option style="" value="3" title="T&#233;cnico em Enfermagem" order="2" visible="visible">
                      T&#233;cnico em Enfermagem
                    </option>
                    <option style="" value="4" title="T&#233;cnico em Est&#233;tica" order="3" visible="visible">
                      T&#233;cnico em Est&#233;tica
                    </option>
                    <option style="" value="5" title="T&#233;cnico em Log&#237;stica" order="4" visible="visible">
                      T&#233;cnico em Log&#237;stica
                    </option>
                    <option style="" value="6" title="T&#233;cnico em Manuten&#231;&#227;o de Microcomputadores" order="5" visible="visible">
                      T&#233;cnico em Manuten&#231;&#227;o de Microcomputadores
                    </option>
                    <option style="" value="7" title="T&#233;cnico em Seguran&#231;a do Trabalho" order="6" visible="visible">
                      T&#233;cnico em Seguran&#231;a do Trabalho
                    </option>
                    <option style="" value="8" title="T&#233;cnico em Inform&#225;tica" order="7" visible="visible">
                      T&#233;cnico em Inform&#225;tica
                    </option>
                  </select>
                </div>
                <div class="form-group col-md-3">
                  <input type="text" name="campoe_11_51" alt="" id="campoe_11_51" easylabel="Telefone" class="form-control" value="" easysync="campoe_11" placeholder="Telefone">
                </div>
              </div>
              <!-- /END Inputs -->

              <!-- Button Submit -->
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="form-group col-md-12">
                  <input type="submit" value="QUERO RECEBER!" class="btn btn-default">
                </div> 
              </div>

            </form> 
          </div>
          <!-- /END Formulário -->

          <!-- Infos -->
          <div class="col-md-12 col-sm-12 col-xs-12 infos">
            <div class="col-md-12">
              <p>CONHEÇA OS CURSOS TÉCNICOS DA ATENEU:</p>
              <a href="http://cursostecnicosfortaleza.com.br/tecnico-em-administracao/" target="_blank">Curso Técnico em Administração</a>,
              <a href="http://cursostecnicosfortaleza.com.br/tecnico-em-edificacoes/" target="_blank">Curso Técnico em Edificações</a>,
              <a href="http://cursostecnicosfortaleza.com.br/tecnico-em-enfermagem/" target="_blank">Curso Técnico em Enfermagem</a>,
              <a href="http://cursostecnicosfortaleza.com.br/tecnico-em-estetica/" target="_blank">Curso Técnico em Estética</a>,
              <a href="http://cursostecnicosfortaleza.com.br/tecnico-em-informatica/" target="_blank">Curso Técnico em Informática</a>,
              <a href="http://cursostecnicosfortaleza.com.br/tecnico-em-logistica/" target="_blank">Curso Técnico em Logística</a>,
              <a href="http://cursostecnicosfortaleza.com.br/tecnico-em-manutencao-de-microcomputadores/" target="_blank">Curso Técnico em Manutenção de Microcomputadores</a>,
              <a href="http://cursostecnicosfortaleza.com.br/tecnico-em-seguranca-trabalho/" target="_blank">Curso Técnico em Segurança do Trabalho</a>.
              </p>
              <p> Para mais informações, você pode entrar em contato pelo telefone <a href="tel:8530335185" target="_blank">85 3033.5185</a> ou pelo e-mail <a href="mailto:cursos.tecnicos@fate.edu.br" target="_blank">cursos.tecnicos@fate.edu.br</a>.
            </div>
          </div>
          <!-- /END Infos -->

          <!-- Footer -->
          <footer>
            <div class="col-md-12 col-sm-12 col-xs-12 copy">
             Cursos Técnicos Ateneu © 2015 Escola Politécnica Ateneu. Todos os direitos reservados.
            </div>
          </footer>
          
          
        </div><!-- /END Table-Cell -->
      </div><!-- /END Table -->
    </div><!-- /END Container -->

    <!-- Modal Mapa -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d127399.73812572398!2d-38.611024607908774!3d-3.7574572005484246!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7c7493a6dfbcd61%3A0xa660578497b8c850!2sCol%C3%A9gio+Piamarta+Montese!5e0!3m2!1spt-BR!2sbr!4v1445363306877" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
        </div><!-- /END.modal-content -->
      </div><!-- /END.modal-dialog -->
    </div><!-- /END Modal mapa -->
    

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- <script src="js/scripts.js"></script> -->
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="js/vendor/holder.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
