<div class="col-md-3 col-sm-3 col-xs-6 box-curso">
  <input order="0" visible="visible" useoutrolabel="Outro (qual?)" easysync="campoe_12" easylabel="Escolha um Curso" id="campoe_12_38_0" type="radio" value="1" title="T&#233;cnico em Administra&#231;&#227;o" name="campoe_12_38" class="curso">
  <label for="campoe_12_38_0" data-theme="grey">
    <div class="table">
      <div class="table-cell">
      Curso Técnico<br> em Administração <br>
      <span class="small">
        <span class="glyphicon glyphicon-map-marker"></span> Antônio Bezerra<br>
        <span class="glyphicon glyphicon-map-marker"></span> Messejana
      </span>
      </div>
    </div>
  </label>
</div>

<div class="col-md-3 col-sm-3 col-xs-6 box-curso"> 
  <input order="1" visible="visible" useoutrolabel="Outro (qual?)" easysync="campoe_12" easylabel="Escolha um Curso" id="campoe_12_38_1" type="radio" value="2" title="T&#233;cnico em Edifica&#231;&#245;es" name="campoe_12_38" class="curso">
  <label for="campoe_12_38_1" data-theme="grey">
    <div class="table">
      <div class="table-cell">
      Curso Técnico<br> em Edificações <br>
      <span class="small">
        <span class="glyphicon glyphicon-map-marker"></span> Antônio Bezerra<br>
        <span class="glyphicon glyphicon-map-marker"></span> Messejana
      </span>
      </div>
    </div>
  </label>
</div>

<div class="col-md-3 col-sm-3 col-xs-6 box-curso"> 
  <input order="2" visible="visible" useoutrolabel="Outro (qual?)" easysync="campoe_12" easylabel="Escolha um Curso" id="campoe_12_38_2" type="radio" value="3" title="T&#233;cnico em Enfermagem" name="campoe_12_38" class="curso">
  <label for="campoe_12_38_2" data-theme="grey">
    <div class="table">
      <div class="table-cell">
      Curso Técnico<br> em Enfermagem <br>
      <span class="small">
        <span class="glyphicon glyphicon-map-marker"></span> Antônio Bezerra<br>
        <span class="glyphicon glyphicon-map-marker"></span> Messejana
      </span>
      </div>
    </div>
  </label>
</div>

<div class="col-md-3 col-sm-3 col-xs-6 box-curso"> 
  <input order="3" visible="visible" useoutrolabel="Outro (qual?)" easysync="campoe_12" easylabel="Escolha um Curso" id="campoe_12_38_3" type="radio" value="4" title="T&#233;cnico em Est&#233;tica" name="campoe_12_38" class="curso">
  <label for="campoe_12_38_3" data-theme="grey">
    <div class="table">
      <div class="table-cell">
      Curso Técnico<br> em Estética <br>
      <span class="small">
        <span class="glyphicon glyphicon-map-marker"></span> Antônio Bezerra<br>
        <span class="glyphicon glyphicon-map-marker"></span> Messejana
      </span>
      </div>
    </div>
  </label>
</div>

<div class="col-md-3 col-sm-3 col-xs-6 box-curso"> 
  <input order="4" visible="visible" useoutrolabel="Outro (qual?)" easysync="campoe_12" easylabel="Escolha um Curso" id="campoe_12_38_7" type="radio" value="8" title="T&#233;cnico em Inform&#225;tica" name="campoe_12_38" class="curso">
  <label for="campoe_12_38_7" data-theme="grey">
    <div class="table">
      <div class="table-cell">
      Curso Técnico<br> em Informática <br>
      <span class="small">
        <span class="glyphicon glyphicon-map-marker"></span> Antônio Bezerra<br>
        <span class="glyphicon glyphicon-map-marker"></span> Messejana
      </span>
      </div>
    </div>
  </label>
</div>

<div class="col-md-3 col-sm-3 col-xs-6 box-curso"> 
  <input order="5" visible="visible" useoutrolabel="Outro (qual?)" easysync="campoe_12" easylabel="Escolha um Curso" id="campoe_12_38_4" type="radio" value="5" title="T&#233;cnico em Log&#237;stica" name="campoe_12_38" class="curso">
  <label for="campoe_12_38_4" data-theme="grey">
    <div class="table">
      <div class="table-cell">
      Curso Técnico<br> em Logística<br>
      <span class="small">
        <span class="glyphicon glyphicon-map-marker"></span> Antônio Bezerra<br>
        <span class="glyphicon glyphicon-map-marker"></span> Messejana
      </span>
      </div>
    </div>
  </label>
</div>

<div class="col-md-3 col-sm-3 col-xs-6 box-curso"> 
  <input order="6" visible="visible" useoutrolabel="Outro (qual?)" easysync="campoe_12" easylabel="Escolha um Curso" id="campoe_12_38_5" type="radio" value="6" title="T&#233;cnico em Manuten&#231;&#227;o de Microcomputadores" name="campoe_12_38" class="curso">
  <label for="campoe_12_38_5" data-theme="grey">
    <div class="table">
      <div class="table-cell">
      Curso Técnico<br> em Manutenção de Microcomputadores<br>
      <span class="small">
        <span class="glyphicon glyphicon-map-marker"></span> Antônio Bezerra<br>
        <span class="glyphicon glyphicon-map-marker"></span> Messejana
      </span>
      </div>
    </div>
  </label>
</div>

<div class="col-md-3 col-sm-3 col-xs-6 box-curso"> 
  <input order="6" visible="visible" useoutrolabel="Outro (qual?)" easysync="campoe_12" easylabel="Escolha um Curso" id="campoe_12_38_6" type="radio" value="7" title="T&#233;cnico em Seguran&#231;a do Trabalho" name="campoe_12_38" class="curso">
  <label for="campoe_12_38_6" data-theme="grey">
    <div class="table">
      <div class="table-cell">
      Curso Técnico<br> em Segurança do Trabalho<br>
      <span class="small">
        <span class="glyphicon glyphicon-map-marker"></span> Antônio Bezerra<br>
        <span class="glyphicon glyphicon-map-marker"></span> Messejana
      </span>
      </div>
    </div>
  </label>
</div>