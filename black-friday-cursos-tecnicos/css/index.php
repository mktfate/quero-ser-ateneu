<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Black Friday Pós-Graduação Ateneu | O Black Friday que dá 100% de Desconto na Matrícula!</title>
    <meta name="description" content="Black Friday Pós-Graduação Ateneu. O Black Friday que dá um mega desconto de 100% matrícula em mais de 25 cursos. Inscreva-se Agora no Black Friday Ateneu!">
    <meta name="keywords" content="black friday, pós-graduação, mega oferta, mba, especialização, black november, 100% desconto, pós-graduação ateneu, faculdade ateneu, oferta especial, carreira.">
    <meta name="author" content="Pós-Graduação Ateneu">
    <meta name="robots" content="noodp,noydir">
    <link rel="shortcut icon" href="images/faviconblack.ico" type="image/x-icon">
    <link rel="icon" href="images/faviconblack.ico" type="image/x-icon">
    <link rel="canonical" href="http://posateneu.com.br/blackfriday">

    <meta property="og:locale" content="pt_BR">
    <meta property="og:type" content="university">
    <meta property="og:title" content="Black Friday | Pós-Graduação Ateneu">
    <meta property="og:description" content="Mais de 25 Cursos de Pós-Graduação, MBA e Especialização">
    <meta property="og:url" content="http://posateneu.com.br/blackfriday">
    <meta property="og:site_name" content="Black Friday | Pós-Graduação Ateneu">
    <meta property="og:image" content="http://posateneu.com.br/blackfriday/images/pos-graduacao-ateneu-black-friday-facebook.jpg">

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">
  </head>

  <body>
    <!-- MAIN -->
    <div class="container">
      <div class="table">
        <div class="table-cell">
          <!-- Logo -->
          <div class="col-md-12 col-sm-12 col-xs-12 logo">
            <picture>
              <img src="images/logo-faculdade-ateneu-montese-white.png" alt="Pós-Graduação Ateneu | Mais de 25 Cursos de Pós-Graduação, MBA e Especialização">
            </picture>
          </div>

          <!-- Destaque -->
          <div class="col-md-12 col-sm-12 col-xs-12 banner">
              <picture>
                <img src="images/black-friday-pos-graduacao-ateneu.jpg" alt="Black Friday Pós-Graduação Ateneu" class="img-responsive">
              </picture>
              <div class="col-md-12 col-sm-12 col-xs-12 line"></div>
              <h1 class="col-md-7 col-sm-12 col-xs-12 textobanner">
                A Pós-Graduação Ateneu também entou na onda do <span class="red-color">Black Friday</span>
                e está oferecendo um <span class="red-color">mega desconto até final do mês de novembro</span> para você começar 2016
                a todo vapor e alcançar de vez sua tão sonhada carreira de sucesso!
              </h1>
              <div class="col-md-5 col-sm-12 col-xs-12 relogio">
                <script>
                var target_date = new Date("november 30, 2015").getTime();
                var dias, horas, minutos, segundos;
                var regressiva = document.getElementById("regressiva");

                setInterval(function () {

                    var current_date = new Date().getTime();
                    var segundos_f = (target_date - current_date) / 1000;

                dias = parseInt(segundos_f / 86400);
                    segundos_f = segundos_f % 86400;
                    
                    horas = parseInt(segundos_f / 3600);
                    segundos_f = segundos_f % 3600;
                    
                    minutos = parseInt(segundos_f / 60);
                    segundos = parseInt(segundos_f % 60);

                    document.getElementById('dia').innerHTML = dias;
                    document.getElementById('hora').innerHTML = horas;
                    document.getElementById('minuto').innerHTML = minutos;
                    document.getElementById('segundo').innerHTML = segundos;

                }, 1000);
                </script>

                <div class="col-md-12 contador">
                  <div class="col-md-3 col-sm-3 col-xs-3 bloco">
                    <p class="legenda">Dias</p>
                    <div class="numero" id="dia"></div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-3 bloco">
                    <p class="legenda">Horas</p>
                    <div class="numero" id="hora"></div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-3 bloco">
                    <p class="legenda">Min</p>
                    <div class="numero" id="minuto"></div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-3 bloco">
                    <p class="legenda">Seg</p>
                    <div class="numero" id="segundo"></div>
                  </div>
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12 line"></div>
              <picture>
                <img src="images/desconto-matricula-e-mensalidade-black-friday-pos-graduacao-ateneu.jpg" alt="100% de Descontona Matrícula + 50% nas 5 Primeiras Mensalidades" class="img-responsive">
              </picture>
              <div class="col-md-12 col-sm-12 col-xs-12 line"></div>
          </div>
          <!-- Close Destaque -->

          <!-- Open Formulário -->
          <div class="col-md-12 col-sm-12 col-xs-12 formulario">
            <form method="post" id="easyform_2e5eDxue3Dla9LQjGe6--7dd7-" class="easyform_2e5eDxue3Dla9LQjGe6--7dd7-" enctype="multipart/form-data" action="http://mkt.queroserateneu.com.br/w/2e5eDxue3Dla9LQjGe6--7dd7-">
              <input type="hidden" name="lista" value="2">
              <input type="hidden" name="cliente" value="138749">
              <input type="hidden" name="lang" id="lang_id" value="br">
              <input type="hidden" name="formid" id="formid" value="5">

              <?php
              $string = basename($_SERVER['QUERY_STRING']);
              $dados = explode('&',$string);
              $total = count($dados);
              $array = '';
              echo '<input type="text" name="campoe_3_32" alt="" id="campoe_3_32" easylabel="Campanha" value="'.$dados[0].'" easysync="campoe_3" style="display:none;">';
              echo '<input type="text" name="campoe_4_33" alt="" id="campoe_4_33" easylabel="Origem" value="'.$dados[1].'" easysync="campoe_4" style="display:none;">';
              ?>

              <!-- Radio Cursos -->
              <div class="col-md-12 col-sm-12 col-xs-12 list-cursos">
                <h2 class="col-md-12">No <span class="red-color">BLACK FRIDAY PÓS-GRADUAÇÃO ATENEU</span>, você escolhe entre mais de 25 cursos de Pós-graduação, MBA´s Executivos e Especialização nas área  do Direito, Educação, Engenharia, Gestão, Saúde e Tecnologia da Informação.</h2>
                <p class="col-md-12">Selecione o seu:</p>

                <div class="panel-group col-md-12 col-sm-12 col-xs-12" id="accordion" role="tablist" aria-multiselectable="true">

                  <div class="panel panel-default">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#gestao" aria-expanded="true" aria-controls="gestao">
                      <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <span class="glyphicon glyphicon-plus"></span> Cursos de Gestão 
                        </h4>
                      </div>
                    </a>
                    <div id="gestao" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                      <div class="panel-body">
                      <?php include("cursos-gestao.php"); ?>
                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#direito" aria-expanded="true" aria-controls="direito">
                      <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                          <span class="glyphicon glyphicon-plus"></span> Cursos de Direito 
                        </h4>
                      </div>
                    </a>
                    <div id="direito" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                      <div class="panel-body">
                      <?php include("cursos-direito.php"); ?>
                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#saude" aria-expanded="true" aria-controls="saude">
                      <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                          <span class="glyphicon glyphicon-plus"></span> Cursos de Saúde 
                        </h4>
                      </div>
                    </a>
                    <div id="saude" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                      <div class="panel-body">
                      <?php include("cursos-saude.php"); ?>
                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#educacao" aria-expanded="true" aria-controls="educacao">
                      <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                          <span class="glyphicon glyphicon-plus"></span> Cursos de Educação 
                        </h4>
                      </div>
                    </a>
                    <div id="educacao" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                      <div class="panel-body">
                      <?php include("cursos-educacao.php"); ?>
                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#engenharia" aria-expanded="true" aria-controls="engenharia">
                      <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                          <span class="glyphicon glyphicon-plus"></span> Cursos de Engenharia, TI e Gestão Ambiental 
                        </h4>
                      </div>
                    </a>
                    <div id="engenharia" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                      <div class="panel-body">
                      <?php include("cursos-engenharia.php"); ?>
                      </div>
                    </div>
                  </div>
                  
                </div>
              </div>
              <!-- /END Radio Cursos -->
      
              <!-- Inputs -->
              <div class="col-md-12 col-sm-12 col-xs-12 fields">
                  <p class="col-md-12">Preencha seus dados:</p>
                <div class="form-group col-md-6">
                  <input type="text" name="fname_27" easylabel="Nome" alt="" id="fname_27" class="form-control" value="" easysync="fname" placeholder="Nome Completo">
                </div>
                <div class="form-group col-md-3">
                  <input type="email" name="email_28" easylabel="E-mail" alt="" id="email_28" class="form-control" value="" easysync="email" easyvalidation="true" placeholder="E-mail">
                </div>
                <div class="form-group col-md-3">
                  <input type="text" name="campoe_1_31" alt="" id="campoe_1_31" easylabel="Telefone" class="form-control" value="" easysync="campoe_1" placeholder="Telefone">
                </div>
              </div>
              <!-- /END Inputs -->

              <!-- Button Submit -->
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="form-group col-md-12">
                  <input type="submit" value="EU QUERO ESSA MEGA OFERTA !" class="btn btn-default">
                </div> 
              </div>

            </form> 
          </div>
          <!-- /END Formulário -->

          <!-- Infos -->
          <div class="col-md-12 col-sm-12 col-xs-12 infos">
            <div class="col-md-12">
              <p class="red-color">Regulamento Mega Promoção Black Friday :</p>
              <p>* A Mega Promoção Black Friday Pós-Graduação Ateneu, oferece 100% de desconto na matrícula + 50% de desconto nas 5 primeiras mensalidades em mais de 25 Cursos de Pós-Graduaçao da Faculdade Ateneu, aos interresados inscritos por meio do formulário oficial desta campanha até 23:59 horas do dia 30/11/2015.</p>
              <p>* A Mega Promoção também só a válida para novos alunos matrículados nas turmas iniciando em 2016 nos seguintes cursos: GESTÃO: 
              <a href="http://posgraduacaofortaleza.com.br/curso/gestao-ambiental/" target="_blank">MBA Gestão Ambiental</a>,
              <a href="http://posgraduacaofortaleza.com.br/curso/mba-em-administracao-de-negocios/" target="_blank">MBA Administração de Negócios</a>,
              <a href="http://posgraduacaofortaleza.com.br/curso/mba-em-controladoria-e-financas/" target="_blank">MBA Controladoria e Finanças</a>,
              <a href="http://posgraduacaofortaleza.com.br/curso/mba-em-gestao-estrategica-de-logistica/" target="_blank">MBA Operações Logísticas</a>,
              <a href="http://posgraduacaofortaleza.com.br/curso/mba-em-gestao-estrategica-de-marketing/" target="_blank">MBA Gestão Estratégica de Marketing</a>,
              <a href="http://posgraduacaofortaleza.com.br/curso/mba-em-administracao-de-recursos-humanos/" target="_blank">MBA Gestão de Recursos Humanos</a>,
              <a href="http://posgraduacaofortaleza.com.br/curso/especializacao-em-gestao-design-de-moda/" target="_blank">MBA Gestão Design de Moda</a>.

              DIREITO:
              <a href="http://posgraduacaofortaleza.com.br/curso/direito-aplicado-ao-setor-pessoal/" target="_blank">ESP. Direito Aplicado ao Setor Pessoal</a>,
              <a href="http://posgraduacaofortaleza.com.br/curso/direito-tributario-trabalhista-e-previdenciario/" target="_blank">ESP. Direito Tributário Trabalhista e Previdenciário</a>,
              <a href="http://posgraduacaofortaleza.com.br/curso/direito-militar/" target="_blank">ESP. Direito Militar</a>,
              <a href="http://posgraduacaofortaleza.com.br/curso/direito-penal/" target="_blank">ESP. Direito Penal</a>,
              <a href="http://posgraduacaofortaleza.com.br/curso/pericia-forense/" target="_blank">ESP. Perícia Forense</a>,
              <a href="#" target="_blank">ESP. Administração Pública</a>.

              SAÚDE:
              <a href="http://posgraduacaofortaleza.com.br/curso/citologia-clinica/" target="_blank">ESP. Citologia Clínica</a>,
              <a href="http://posgraduacaofortaleza.com.br/curso/farmacologia/" target="_blank">ESP. Farmacologia</a>,
              <a href="http://posgraduacaofortaleza.com.br/curso/gerontologia/" target="_blank">ESP. Gerontologia</a>,
              <a href="http://posgraduacaofortaleza.com.br/curso/saude-da-familia/" target="_blank">ESP. Saúde da Família</a>,
              <a href="http://posgraduacaofortaleza.com.br/curso/seguranca-alimentar-e-nutricional/" target="_blank">ESP. Segurança Alimentar e Nutricional</a>,
              <a href="http://posgraduacaofortaleza.com.br/curso/vigilancia-sanitaria/" target="_blank">ESP. Vigilância Sanitária</a>.

              EDUCAÇÃO:
              <a href="http://posgraduacaofortaleza.com.br/curso/cultura-e-historia-afro-brasileira/" target="_blank">ESP. Cultura e História Afro-Brasileira</a>,
              <a href="http://posgraduacaofortaleza.com.br/curso/ensino-de-espanhol/" target="_blank">ESP. Ensino de Espanhol</a>,
              <a href="http://posgraduacaofortaleza.com.br/curso/gestao-escolar/" target="_blank">ESP. Gestão Escolar</a>,
              <a href="http://posgraduacaofortaleza.com.br/curso/informatica-na-educacao/" target="_blank">ESP. Informatica na Educação</a>,
              <a href="http://posgraduacaofortaleza.com.br/curso/lingua-portuguesa-e-literatura/" target="_blank">ESP. Língua Portuguesa e Literatura</a>,
              <a href="http://posgraduacaofortaleza.com.br/curso/psicopedagogia/" target="_blank">ESP. Psicopedagogia</a>.

              ENGENHARIA E TI: 
              <a href="#" target="_blank">ESP. Engenharia de Segurança no Trabalho</a>,
              <a href="http://posgraduacaofortaleza.com.br/curso/governanca-de-ti/" target="_blank">ESP. Governança de TI</a>,
              <a href="http://posgraduacaofortaleza.com.br/curso/seguranca-em-redes-de-computadores/" target="_blank">ESP. Segurança em Redes de Computadores</a>,
              <a href="http://posgraduacaofortaleza.com.br/curso/mba-gerenciamento-de-projetos/" target="_blank">MBA Gerenciamento de Projetos</a>.
              </p>

              <p>*Promoção com vagas limitadas e não cumulativa com outros programas de descontos e bolsas da Pós-Graduação Ateneu.</p>
            </div>
          </div>
          <!-- /END Infos -->

          <!-- Footer -->
          <footer>
            <div class="col-md-12 col-sm-12 col-xs-12 copy">
              © 2015 Faculdade Ateneu. Todos os direitos reservados.
            </div>
          </footer>
          
          
        </div><!-- /END Table-Cell -->
      </div><!-- /END Table -->
    </div><!-- /END Container -->

    <!-- Modal Mapa -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d127399.73812572398!2d-38.611024607908774!3d-3.7574572005484246!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7c7493a6dfbcd61%3A0xa660578497b8c850!2sCol%C3%A9gio+Piamarta+Montese!5e0!3m2!1spt-BR!2sbr!4v1445363306877" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
        </div><!-- /END.modal-content -->
      </div><!-- /END.modal-dialog -->
    </div><!-- /END Modal mapa -->
    

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- <script src="js/scripts.js"></script> -->
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="js/vendor/holder.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
