﻿<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Parabéns! Seja bem-vindo ao Black Friday Cursos Técnicos Ateneu.">
    <meta name="author" content="Escola Politécnica Ateneu">
    <meta name="robots" content="noindex"/>
    <link rel="shortcut icon" href="images/faviconblack.ico" type="image/x-icon">
    <link rel="icon" href="images/faviconblack.ico" type="image/x-icon">
    <link rel="canonical" href="http://queroserateneu.com.br/black-friday-cursos-tecnicos/confirma-cadastro.php">

    <title>Parabéns! Seja bem-vindo ao Black Friday Cursos Técnicos Ateneu.</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">

  </head>
  <body>

    <!-- MAIN -->
    <div class="container">
      <div class="table">
        <div class="table-cell">
          <!-- Logo -->
          <div class="col-md-12 logo">
            <picture>
              <img src="images/logo-faculdade-ateneu-montese-white.png" alt="Escola Politecnica Ateneu | 8 Cursos Técnicos em Fortaleza">
            </picture>
          </div>

          <!-- Open Formulário -->
          <div class="col-md-12 formulario confirma">
            <div class="col-md-12">
              <h4><span class="yellow-color">PARABÉNS!<br> SEJA BEM VINDO A<br>ESCOLA POLITÉCNICA ATENEU.</span></h4><br>
              <p>Agora você não vai mais perder nenhuma de nossas promoções!<br> Acesse o seu e-mail e siga as nossas instruções para confirmar seu cadastro.</p><br>
              <p>Acessar e-mail:<br><br>
              <a href="https://mail.google.com/" target="_blank"><img src="images/gmail-icon.png" alt="Acessar Gmail"></a>
              <a href="https://login.live.com/" target="_blank"><img src="images/outlook-icon.png" alt="Acessar Outlook"></a>
              <a href="https://mail.yahoo.com.br/" target="_blank"><img src="images/yahoo-icon.png" alt="Acessar Yahoo Mail"></a>
              </p>
            </div>
          </div>
          <!-- /END Formulário -->

          <!-- Destaque -->
          <div class="col-md-12 banner">
              <picture>
                <img src="images/black-friday-cursos-tecnicos-ateneu.jpg" alt="Black Friday Cursos Técnicos Ateneu" class="img-responsive">
              </picture>
              <div class="col-md-12 line"></div>
          </div>
          <!-- Close Destaque -->

          <!-- Infos -->
          <div class="col-md-12 col-sm-12 col-xs-12 infos">
            <div class="col-md-12">
              <p>CONHEÇA OS CURSOS TÉCNICOS DA ATENEU:</p>
              <a href="http://cursostecnicosfortaleza.com.br/tecnico-em-administracao/" target="_blank">Curso Técnico em Administração</a>,
              <a href="http://cursostecnicosfortaleza.com.br/tecnico-em-edificacoes/" target="_blank">Curso Técnico em Edificações</a>,
              <a href="http://cursostecnicosfortaleza.com.br/tecnico-em-enfermagem/" target="_blank">Curso Técnico em Enfermagem</a>,
              <a href="http://cursostecnicosfortaleza.com.br/tecnico-em-estetica/" target="_blank">Curso Técnico em Estética</a>,
              <a href="http://cursostecnicosfortaleza.com.br/tecnico-em-informatica/" target="_blank">Curso Técnico em Informática</a>,
              <a href="http://cursostecnicosfortaleza.com.br/tecnico-em-logistica/" target="_blank">Curso Técnico em Logística</a>,
              <a href="http://cursostecnicosfortaleza.com.br/tecnico-em-manutencao-de-microcomputadores/" target="_blank">Curso Técnico em Manutenção de Microcomputadores</a>,
              <a href="http://cursostecnicosfortaleza.com.br/tecnico-em-seguranca-trabalho/" target="_blank">Curso Técnico em Segurança do Trabalho</a>.
              </p>
              <p> Para mais informações, você pode entrar em contato pelo telefone <a href="tel:8530335185" target="_blank">85 3033.5185</a> ou pelo e-mail <a href="mailto:cursos.tecnicos@fate.edu.br" target="_blank">cursos.tecnicos@fate.edu.br</a>.
            </div>
          </div>
          <!-- /END Infos -->

          <!-- Footer -->
          <footer>
            <div class="col-md-12 col-sm-12 col-xs-12 copy">
              Cursos Técnicos Ateneu © 2015 Escola Politécnica Ateneu. Todos os direitos reservados.
            </div>
          </footer>
          
        </div><!-- /END Table-Cell -->
      </div><!-- /END Table -->
    </div><!-- /END Container -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- <script src="js/scripts.js"></script> -->
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="js/vendor/holder.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
