﻿<!DOCTYPE html>
<!--[if lt IE 7 ]><html
lang="pt-BR" prefix="og: http://ogp.me/ns#" class="ie6"> <![endif]-->
<!--[if IE 7 ]><html
lang="pt-BR" prefix="og: http://ogp.me/ns#" class="ie7"> <![endif]-->
<!--[if IE 8 ]><html
lang="pt-BR" prefix="og: http://ogp.me/ns#" class="ie8"> <![endif]-->
<!--[if IE 9 ]><html
lang="pt-BR" prefix="og: http://ogp.me/ns#" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="pt-BR" ng-app="myApp" xmlns:fb="http://ogp.me/ns/fb#" prefix="og: http://ogp.me/ns#" class=" js flexbox webgl no-touch geolocation hashchange history websockets rgba hsla multiplebgs backgroundsize borderimage textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage applicationcache svg svgclippaths mediaqueries no-regions supports"><!--<![endif]-->
  <head>

    <!-- CSS -->
    <link href="../assets/css/main.min.css" rel="stylesheet">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Resultado Enem Faculdade Ateneu</title>
    <meta name="description" content="Estude na Ateneu com o seu resultado do ENEM. Use o seu ENEM a partir de 2010.">
    <meta name="keywords" content="enem, enem 2015, resultado enem, prova enem, nota enem, sisu, mec, minist&eacute;rio educa&ccedil;&atilde;o, inep, faculdade ateneu, faculdade fortaleza">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="content-language" content="pt-br" />
    <meta name="copyright" content="© 2016 Faculdade Ateneu" />
    <meta name="rating" content="general" />
    <meta name="author" content="Faculdade Ateneu">
    <meta name="robots" content="noodp,noydir">
    <link rel="shortcut icon" href="../assets/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="../assets/images/favicon.ico" type="image/x-icon">
    <link href="http://queroserateneu.com.br/resultado-enem/darkpostenem.png" rel="image_src" />

    <meta property="og:locale" content="pt_BR">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Estude na Ateneu com o seu Resultado do ENEM.">
    <meta property="og:description" content="Especialize-se em diversas áreas e tenha o mercado mais favorável na sua carreira de sucesso.">
    <meta property="og:url" content="http://queroserateneu.com.br/resultado-enem/?<?php $string = basename($_SERVER['QUERY_STRING']); echo $string ?>">
    <meta property="og:site_name" content="Faculdade Ateneu">
    <meta property="og:image" content="http://queroserateneu.com.br/resultado-enem/darkpostenem.png">
    <meta property="og:image:width" content="800">
    <meta property="og:image:height" content="500">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


  </head>
  <body>
    <!-- MAIN -->
    <div class="container">
      <!-- <div class="table"> -->
        <!-- <div class="table-cell"> -->
          <!-- Destaque -->
          <div class="col-md-4 col-sm-12 col-xs-12 destaque hidden-xs hidden-sm">
            <div class="col-md-12 col-sm-4 hidden-xs imagem">
              <picture>
                <img src="../assets/images/resultado-enem-cursos-graduacao-faculdade-ateneu.jpg" alt="Ingresse na Faculdade Ateneu com o Resulado do ENEM">
              </picture>
              <!-- <h2>USE A NOTA DO ENEM<br> E FOQUE SUA CARREIRA NO<br> <span class="blue-color">SUCESSO PROFISSIONAL.</span></h2> -->
            </div>
          </div>

          <!-- Formularios -->
          <div class="col-md-8 col-sm-12 col-xs-12 formulario" ng-controller="FormController">
            <div class="row">
            <!-- Webservice -->
            <form name="myForm" id="webservice" action="" method="POST" target="action" novalidate ng-submit="mandaForm()">
                <input type="text" size="50" name="Acesso" style="display: none;" value="6CgvsiW0D2HSsSa1foEdaQ==">
                <input type="text" size="50" name="IDExterno" style="display: none;" value="">
                <input type="text" size="50" name="RG" value="" style="display: none;">
                <input type="text" size="50" name="DataNasc" value="" style="display: none;">
                <input type="text" size="50" name="Cep" value="" style="display: none;">
                <input type="text" size="50" name="Endereco" value="" style="display: none;">
                <input type="text" size="50" name="Numero" value="" style="display: none;">
                <input type="text" size="50" name="Complemento" value="" style="display: none;">
                <input type="text" size="50" name="Bairro" value="" style="display: none;">
                <input type="text" size="50" name="Cidade" value="" style="display: none;">
                <input type="text" size="50" name="Estado" value="" style="display: none;">
                <input type="text" size="50" name="Deficiencia" value="" style="display: none;">
                <input type="text" size="50" name="DescricaoDeficiencia" value="" style="display: none;">
                <input type="text" size="50" name="DataProva" value="" style="display: none;" >
                <input type="text" size="50" name="CheckInMailer" value="" style="display: none;">
                <input type="text" size="50" name="Periodo" value="" style="display: none;">
                <input type="text" size="50" name="Valor" value="" style="display: none;">

                <!-- Tipo de inscrição -->
                <input type="text" size="50" name="TipoInscricao" value="enem" style="display:none;">

                <?php
                  // Pegar Query Strings
                  $string = basename($_SERVER['QUERY_STRING']);
                  $dados = explode('&',$string);
                  $total = count($dados);
                  $array = '';

                  $campanha = ltrim(strstr($dados[0], '='), '=');
                  $midia = ltrim(strstr($dados[1], '='), '=');
                  $consultor = ltrim(strstr($dados[2], '='), '=');

                  //Campanha - Hidden
                  echo '<input type="text" name="Campanha" value="'.$campanha.'" style="display:none;">';
                  echo '<input type="text" name="Midia" value="'.$midia.'" style="display:none;">';
                ?>

              <!-- Dados Pessoais -->
              <div class="col-md-12">
                <div class="col-md-12"><h1 class="titulo-enem"><span class="blue-color">ESTUDE NA ATENEU COM O</span> <br class="visible-xs"><span class="label">RESULTADO DO ENEM</span></h1></div>
                <!-- <div class="col-md-12"><h4>Dados Pessoais:</h4></div> -->
                <p class="hidden-xs hidden-sm"><br><br><br></p>
                  <p class="visible-sm"><br></p>

                <div class="form-group col-md-4">
                  <input class="form-control" type="text" name="Nome" id="nome" ng-model="nome" placeholder="Nome" required>
                  <label ng-show="myForm.Nome.$invalid && !myForm.Nome.$pristine" class="error">Por favor, digite seu nome!</label>
                </div>

                <div class="form-group col-md-4">
                  <input class="form-control" type="email" name="Email" id="email" ng-model="email" placeholder="E-mail" required>
                  <label ng-show="myForm.Email.$invalid && !myForm.Email.$pristine" class="error">Por favor, digite seu email!</label>
                </div>

                <div class="form-group col-md-4">
                  <input class="form-control" type="text" name="Telefone" id="telefone" ng-model="telefone" angular-mask="(00)00000-0000" placeholder="Telefone" ng-minlength="14" required>
                  <label ng-show="myForm.Telefone.$invalid && !myForm.Telefone.$pristine"  class="error">Por favor, informe o seu telefone!</label>
                  <label ng-show="myForm.Telefone.$error.minlength" class="error">Minimo de 11 caracteres</label>
                </div>

              </div>
              <!-- /END Dados Pessoais -->

              <div class="col-md-12">
              <!-- <div class="col-md-12"><h4>Escolha o seu Curso:</h4></div> -->
                <!-- Unidade -->
                <div class="form-group col-md-6">
                  <select class="form-control unidade" name="Unidade" id="unidade" ng-model="unidade" required>
                    <option value="">Selecione uma Unidade</option>
                    <option ng-repeat="unidade in unidades" value="{{unidade.value}}">{{unidade.name}}</option>
                  </select>
                  <label ng-show="!myForm.Unidade.$invalid && myForm.Unidade.$pristine" class="error">*Por favor, selecione uma Unidade Ateneu!</label>
                </div>

                <!-- Cursos -->
                <div class="form-group col-md-6" ng-switch="unidade">
                  <select class="form-control" name="Curso" id="curso" ng-model="curso.value" ng-switch-when="1" required placeholder="teste">
                    <option selected="selected" selected="selected" ng-repeat="ab in CursosAntonioBezerra" value="{{ab.value}}" order="1" visible="visible">
                      {{ab.name}}
                    </option>
                  </select>

                  <select class="form-control" name="Curso" id="curso" ng-model="curso.value" ng-switch-when="2" required>
                    <option selected="selected" ng-repeat="m in CursosMessejana" value="{{m.value}}" order="0" visible="visible">
                      {{m.name}}
                    </option>
                  </select>

                  <select class="form-control" name="Curso" id="curso" ng-model="curso.value" ng-switch-when="3" required>
                    <option selected="selected" ng-repeat="p in CursosPecem" value="{{p.value}}" order="0" visible="visible">
                      {{p.name}}
                    </option>
                  </select>

                  <select class="form-control" name="Curso" id="curso" ng-model="curso.value" ng-switch-when="4" required>
                    <option selected="selected" ng-repeat="mont in CursosMontese" value="{{mont.value}}" order="0" visible="visible">
                      {{mont.name}}
                    </option>
                  </select>

                  <select class="form-control" name="Curso" id="curso" ng-switch-default>
                    <option value="Selecione uma Unidade Ateneu">Selecione uma Unidade Ateneu</option>
                  </select>
                  <label ng-show="!myForm.Curso.$invalid && myForm.Curso.$pristine" class="error">*Primeiro selecione uma unidade.</label>
                </div>
              </div>

              <div id="mensagens"></div>

              <div class="col-md-12">
                <div class="form-group col-md-12">
                  <button type="submit" id="enviar" class="btn btn-default" ng-disabled="myForm.$invalid">Inscreva-se Agora!</button>
                </div>
              </div>

              <!-- Open Oferta -->
              <div class="col-md-12 visible-lg desconto">
                 <!-- Infos -->
                <div class="col-md-12 col-sm-12 col-xs-12 infos">
                  <div class="col-md-12 col-sm-12 col-xs-12">

                  <div class="col-md-4 col-sm-4 col-xs-6 info-bloco">
                    <div class="col-md-3 col-sm-4 col-xs-4 icon">
                      <div class="table">
                        <div class="table-cell">
                          <i class="glyphicon glyphicon-star"></i>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8 content">
                      <div class="table">
                        <div class="table-cell">
                          <span class="blue-color">90% de Desconto</span><br>na matr&iacute;cula<sup>1</sup>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-4 col-sm-4 col-xs-6 info-bloco">
                    <div class="col-md-3 col-sm-4 col-xs-4 icon">
                      <div class="table">
                        <div class="table-cell">
                          <i class="glyphicon glyphicon-education"></i>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8 content">
                      <div class="table">
                        <div class="table-cell">
                          <span class="blue-color">50% de Desconto na</span><br>Primeira Mensalidade<sup>2</sup>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-4 col-sm-4 col-xs-6 info-bloco">
                    <div class="col-md-3 col-sm-4 col-xs-4 icon">
                      <div class="table">
                        <div class="table-cell">
                          <i class="glyphicon glyphicon-usd"></i>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8 content">
                      <div class="table">
                        <div class="table-cell">
                          <span class="blue-color">30% da 2ª a 5ª</span><br>Mensalidade<sup>2</sup>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
                </div>
                <!-- /END Infos -->
              </div>
              <!-- /END Oferta -->
            </form>
            <!--/END Webservice -->

            <!-- E-goi -->
            <form class="hidden" id="e-goi" method="post" enctype="multipart/form-data" action="http://mkt.queroserateneu.com.br/w/1e10eDxue7bfwcfP5He578d6186" target="action2">
              <input type="hidden" name="lista" value="1">
              <input type="hidden" name="cliente" value="138749">
              <input type="hidden" name="lang" id="lang_id" value="br">
              <input type="hidden" name="formid" id="formid" value="60">
              <input name="fname_586" id="fname_586" value="{{nome}}" type="text">
              <input name="email_587" id="email_587" value="{{email}}" type="email" easyvalidation="false">
              <input name="campoe_15_591" id="campoe_15_591" value="{{telefone}}" type="text">
              <input type="text" name="campoe_34_605" id="campoe_34_605" value="{{cpf}}">

              <!-- FORMA DE INGRESSO | ENEM -->
              <input type="text" name="campoe_19_588" id="campoe_19_588_1" value="2">

              <input type="text" name="campoe_20_589" id="campoe_20" value="{{unidade}}">
              <input type="text" name="campoe_18_590" id="campoe_18" value="{{curso.value}}">

              <?php
                // Pegar URL
                $urlfull = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                $url = explode('/', $urlfull);
                $urlfinal = "$url[0]/$url[1]";

                // Pegar Query Strings
                $string = basename($_SERVER['QUERY_STRING']);
                $dados = explode('&',$string);
                $total = count($dados);
                $array = '';

                $campanha = ltrim(strstr($dados[0], '='), '=');
                $midia = ltrim(strstr($dados[1], '='), '=');
                $consultor = ltrim(strstr($dados[2], '='), '=');

                //Campanha - Hidden
                echo '<input type="text" name="campoe_16_592" id="campoe_16_592" value="'.$campanha.'">';
                echo '<input type="text" name="campoe_43_593" id="campoe_43_593" value="'.$midia.'">';
                echo '<input type="text" name="campoe_30_594" id="campoe_30_594" value="'.$consultor.'">';
                echo '<input type="text" name="campoe_17_601" id="campoe_17_601" value="'.$urlfinal.'">';
              ?>
            </form>

            <iframe class="hidden" src="action.php" name="action" frameborder="0"></iframe>
            <iframe class="hidden" src="action2.php" name="action2" frameborder="0"></iframe>

          </div>
          </div>
          <!--/END E-goi  -->
          <!-- /END Formul&#225;rio -->

          <!-- Open Oferta -->
          <div class="col-md-12 hidden-lg desconto">
             <!-- Infos -->
                <div class="col-md-12 col-sm-12 col-xs-12 infos">
                  <div class="col-md-12 col-sm-12 col-xs-12">

                  <div class="col-md-4 col-sm-4 col-xs-6 info-bloco">
                    <div class="col-md-3 col-sm-4 col-xs-4 icon">
                      <div class="table">
                        <div class="table-cell">
                          <i class="glyphicon glyphicon-star"></i>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8 content">
                      <div class="table">
                        <div class="table-cell">
                          <span class="blue-color">90% de Desconto</span><br>na matr&iacute;cula<sup>1</sup>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-4 col-sm-4 col-xs-6 info-bloco">
                    <div class="col-md-3 col-sm-4 col-xs-4 icon">
                      <div class="table">
                        <div class="table-cell">
                          <i class="glyphicon glyphicon-education"></i>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8 content">
                      <div class="table">
                        <div class="table-cell">
                          <span class="blue-color">50% de Desconto na</span><br>Primeira Mensalidade<sup>2</sup>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-4 col-sm-4 col-xs-6 info-bloco">
                    <div class="col-md-3 col-sm-4 col-xs-4 icon">
                      <div class="table">
                        <div class="table-cell">
                          <i class="glyphicon glyphicon-usd"></i>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8 content">
                      <div class="table">
                        <div class="table-cell">
                          <span class="blue-color">30% da 2ª a 5ª</span><br>Mensalidade<sup>2</sup>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
                </div>
                <!-- /END Infos -->
          </div>
          <!-- /END Oferta -->

       <!--  </div> --><!-- /END Table-Cell -->
      <!-- </div> --><!-- /END Table -->
    </div><!-- /END Container -->

    <!-- Footer -->
    <footer class="container-fluid">
      <div class="container copy">
        <div class="col-md-12">
          <h3 style="text-align: center;"><strong>Fa&#231;a Sua Gradua&#231;&#227;o na Faculdade Ateneu Utilizando Seu Resultado do ENEM.</strong></h3>
          <p style="text-align: center;">Ao ingressar na <strong>Faculdade Ateneu com o seu Resultado do ENEM</strong>, voc&#234; tem al&#233;m de um curr&#237;culo moderno e professores de qualidade, diversos cursos de extens&#227;o. &#201; a sua oportunidade de se especializar em diversos campos e ter um mercado de trabalho muito mais favor&#225;vel quando sair com diploma na m&#227;o. Entendeu?.</p>
        </div>
        <div class="col-md-6">
          <!-- Matricula -->
          <p><h4><strong>Regulamento e Condi&#231;&#245;es:</strong></h4></p>
          <p>Usando o seu <strong>Resultado do ENEM na Faculdade Ateneu</strong>, voc&ecirc; tem a sua disposi&#231;&#227;o mais de 25 cursos de gradua&ccedil;&atilde;o para come&#231;ar a construir o seu futuro profissional em 2016. Se voc&ecirc; n&atilde;o tiver zerado a reda&ccedil;&atilde;o e nenhuma das demais &aacute;reas na prova do ENEM, voc&ecirc; pode ingressar na Faculdade Ateneu usando a sua nota do Exame Nacional do Ensino M&eacute;dio a partir de 2010. Outro ponto importante sobre o ENEM, &eacute; que al&eacute;m da nota obtida no exame substituir a prova do vestibular, o ENEM ainda &eacute; um recurso para participar dos programas federais de ensino, como: <a href="http://fies.fate.edu.br/" target="_blank">FIES</a> e <a href="http://fate.edu.br/prouni" target="_blank">PROUNI</a>.
          </p>
          <div class="line"></div>
          <p><sup>1</sup> Desconto exclusivo de 90% na matr&#237;cula de novatos nos <strong>Cursos de Gradua&#231;&#227;o da Faculdade Ateneu</strong> para o semestre de 2017.1, efetuadas at&#233; o dia 24 de Outubro de 2016 atrav&#233;s do <strong>Resultado do ENEM.</strong></p>
          <div class="line"></div>
          
          <!-- Promoção ENEM -->
          <p><sup>2</sup> Desconto Especial de 50% na primeira + 30% da 2ª a 5ª mensalidade dos <strong>Cursos de Gradua&#231;&#227;o da Faculdade Ateneu</strong>, referente a matr&#237;culas de novatos efetuadas at&#233; o dia 24 de Outubro de 2016 atrav&#233;s do <strong>Resultado do ENEM</strong>, com exceção das taxas de rematrícula a partir do segundo semestre.</p>
          <div class="line"></div>
          
          <!-- Promo&#231;&#227;o Pagamento Adiantado -->
          <p class="blue-color"><strong>Ganhe 5% desconto nas mensalidades pagas at&#233; o dia 5 de cada m&#234;s.</strong></p>
          <div class="line"></div>
          <p>*Promo&#231;&#245;es n&#227;o cumulativas com outras promo&#231;&#245;es e bolsas da Faculdade Ateneu.</p>
          <div class="line"></div>
          <p><h4><strong>Documentos Ingresso Via ENEM:</strong></h4></p>
          <p>
            <i class="glyphicon glyphicon-file"></i> Edital ENEM | <a href="http://fate.edu.br/documentos/faculdade-ateneu-edital-ingresso-via-enem-20162.pdf" class="bt" target="_blank" ><i class="glyphicon glyphicon-chevron-right"></i> Ver Edital</a> <br>
            <i class="glyphicon glyphicon-file"></i> Manual do Aluno | <a href="http://fate.edu.br/documentos/manual_informacoes_novatos_20152.pdf" class="bt" target="_blank" ><i class="glyphicon glyphicon-chevron-right"></i> Ver Manual</a>
          </p>
        </div>
      
        <div class="col-md-6">
          <p><h4><strong>Unidades Faculdade Ateneu:</strong></h4></p>
          <p>
            <i class="glyphicon glyphicon-map-marker"></i> Messejana: <a href="#" class="bt" data-toggle="modal" data-target="#myModalMessejana"><i class="glyphicon glyphicon-chevron-right"></i> Ver Mapa</a><br>
            <i class="glyphicon glyphicon-map-marker"></i> Ant&#244;nio Bezerra: <a href="#" class="bt" data-toggle="modal" data-target="#myModalAntBezerra"><i class="glyphicon glyphicon-chevron-right"></i> Ver Mapa</a><br>
            <i class="glyphicon glyphicon-map-marker"></i> Montese: <a href="#" class="bt" data-toggle="modal" data-target="#myModalMontese"><i class="glyphicon glyphicon-chevron-right"></i> Ver Mapa</a><br>
            <i class="glyphicon glyphicon-map-marker"></i> Pec&#233;m: <a href="#" class="bt" data-toggle="modal" data-target="#myModalPecem"><i class="glyphicon glyphicon-chevron-right"></i> Ver Mapa</a>
          </p>
          <div class="line"></div>

          <h4><strong>Cursos de Gradua&#231;&#227;o em Fortaleza | Faculdade Ateneu Messejana</strong></h4>
          <p><a href="http://fate.edu.br/curso/administracao" target="_blank">Administra&ccedil;&atilde;o</a>,
          <a href="http://fate.edu.br/curso/analise-e-desenvolvimento-de-sistemas" target="_blank">An&aacute;lise e Desenvolvimento de Sistemas</a>,
          <a href="http://fate.edu.br/curso/arquitetura-e-urbanismo/" target="_blank">Arquitetura e Urbanismo</a>,
          <a href="http://fate.edu.br/curso/ciencias-contabeis" target="_blank">Ci&ecirc;ncias Cont&aacute;beis</a>,
          <a href="http://fate.edu.br/curso/design-de-moda" target="_blank">Design de Moda</a>,
          <a href="http://fate.edu.br/curso/educacao-fisica" target="_blank">Educa&ccedil;&atilde;o F&iacute;sica</a>,
          <a href="http://fate.edu.br/curso/enfermagem" target="_blank">Enfermagem</a>,
          <a href="http://fate.edu.br/curso/engenharia-civil" target="_blank">Engenharia Civil</a>,
          <a href="http://fate.edu.br/curso/engenharia-de-producao" target="_blank">Engenharia de Produ&ccedil;&atilde;o</a>,
          <a href="http://fate.edu.br/curso/fisioterapia" target="_blank">Fisioterapia</a>,
          <a href="http://fate.edu.br/curso/gestao-de-turismo" target="_blank">Gest&atilde;o de Turismo</a>,
          <a href="http://fate.edu.br/curso/gestao-financeira" target="_blank">Gest&atilde;o Financeira</a>,
          <a href="http://fate.edu.br/curso/logistica" target="_blank">Log&iacute;stica</a>,
          <a href="http://fate.edu.br/curso/marketing" target="_blank">Marketing</a>,
          <a href="http://fate.edu.br/curso/nutricao" target="_blank">Nutri&ccedil;&atilde;o</a>,
          <a href="http://fate.edu.br/curso/pedagogia" target="_blank">Pedagogia</a>,
          <a href="http://fate.edu.br/curso/processos-gerenciais" target="_blank">Processos Gerenciais</a>,
          <a href="http://fate.edu.br/curso/psicologia" target="_blank">Psicologia</a>,
          <a href="http://fate.edu.br/curso/recursos-humanos" target="_blank">Recursos Humanos</a>,
          <a href="http://fate.edu.br/curso/redes-de-computadores" target="_blank">Redes de Computadores</a>,
          <a href="http://fate.edu.br/curso/secretariado" target="_blank">Secretariado</a>,
          <a href="http://fate.edu.br/curso/servico-social" target="_blank">Servi&ccedil;o Social</a>.
          </p>
          <div class="line"></div>
          <h4><strong>Cursos de Gradua&#231;&#227;o em Fortaleza | Faculdade Ateneu Ant&#244;nio Bezerra</strong></h4>
          <p><a href="http://fate.edu.br/curso/administracao" target="_blank">Administra&ccedil;&atilde;o</a>,
          <a href="http://fate.edu.br/curso/analise-e-desenvolvimento-de-sistemas" target="_blank">An&aacute;lise e Desenvolvimento de Sistemas</a>,
          <a href="http://fate.edu.br/curso/ciencias-contabeis" target="_blank">Ci&ecirc;ncias Cont&aacute;beis</a>,
          <a href="http://fate.edu.br/curso/educacao-fisica" target="_blank">Educa&ccedil;&atilde;o F&iacute;sica</a>,
          <a href="http://fate.edu.br/curso/enfermagem" target="_blank">Enfermagem</a>,
          <a href="http://fate.edu.br/curso/engenharia-civil" target="_blank">Engenharia Civil</a>,
          <a href="http://fate.edu.br/curso/engenharia-de-producao" target="_blank">Engenharia de Produ&ccedil;&atilde;o</a>,
          <a href="http://fate.edu.br/curso/estetica-e-cosmetica" target="_blank">Est&eacute;tica e Cosm&eacute;tica</a>,
          <a href="http://fate.edu.br/curso/fisioterapia" target="_blank">Fisioterapia</a>,
          <a href="http://fate.edu.br/curso/gestao-de-turismo" target="_blank">Gest&atilde;o de Turismo</a>,
          <a href="http://fate.edu.br/curso/gestao-financeira" target="_blank">Gest&atilde;o Financeira</a>,
          <a href="http://fate.edu.br/curso/logistica" target="_blank">Log&iacute;stica</a>,
          <a href="http://fate.edu.br/curso/pedagogia" target="_blank">Pedagogia</a>,
          <a href="http://fate.edu.br/curso/processos-gerenciais" target="_blank">Processos Gerenciais</a>,
          <a href="http://fate.edu.br/curso/psicologia" target="_blank">Psicologia</a>,
          <a href="http://fate.edu.br/curso/recursos-humanos" target="_blank">Recursos Humanos</a>,
          <a href="http://fate.edu.br/curso/redes-de-computadores" target="_blank">Redes de Computadores</a>,
          <a href="http://fate.edu.br/curso/secretariado" target="_blank">Secretariado</a>,
          <a href="http://fate.edu.br/curso/servico-social" target="_blank">Servi&ccedil;o Social</a>.
          </p>
          <div class="line"></div>
          <h4><strong>Cursos de Gradua&#231;&#227;o em Fortaleza | Faculdade Ateneu Montese</strong></h4>
          <p><a href="http://fate.edu.br/curso/administracao" target="_blank">Administra&ccedil;&atilde;o</a>,
          <a href="http://fate.edu.br/curso/analise-e-desenvolvimento-de-sistemas" target="_blank">An&aacute;lise e Desenvolvimento de Sistemas</a>,
          <a href="http://fate.edu.br/curso/ciencias-contabeis" target="_blank">Ci&ecirc;ncias Cont&aacute;beis</a>,
          <a href="http://fate.edu.br/curso/fisioterapia" target="_blank">Fisioterapia</a>,
          <a href="http://fate.edu.br/curso/logistica" target="_blank">Log&iacute;stica</a>,
          <a href="http://fate.edu.br/curso/recursos-humanos" target="_blank">Recursos Humanos</a>,
          <a href="http://fate.edu.br/curso/redes-de-computadores" target="_blank">Redes de Computadores</a>.
          </p>
          <div class="line"></div>
          <h4><strong>Cursos de Gradua&#231;&#227;o no Pec&#233;m | Faculdade Ateneu </strong></h4>
          <p><a href="http://pecem.fate.edu.br/curso/administracao/" target="_blank">Administra&ccedil;&atilde;o</a>,
          <a href="http://pecem.fate.edu.br/curso/analise-e-desenvolvimento-de-sistemas/" target="_blank">An&aacute;lise e Desenvolvimento de Sistemas</a>,
          <a href="http://pecem.fate.edu.br/curso/comercio-exterior/" target="_blank">Com&eacute;rcio Exterior</a>,
          <a href="http://pecem.fate.edu.br/curso/enfermagem/" target="_blank">Enfermagem</a>,
          <a href="http://pecem.fate.edu.br/curso/gestao-de-recursos-humanos/" target="_blank">Recursos Humanos</a>,
          <a href="http://pecem.fate.edu.br/curso/gestao-portuaria/" target="_blank">Gest&atilde;o Portu&aacute;ria</a>,
          <a href="http://pecem.fate.edu.br/curso/gestao-de-turismo/" target="_blank">Gest&atilde;o de Turismo</a>,
          <a href="http://pecem.fate.edu.br/curso/redes-de-computadores/" target="_blank">Redes de Computadores</a>
          <a href="http://pecem.fate.edu.br/curso/pedagogia/" target="_blank">Pedagogia</a>.
          </p>
          </div> 
          
          <div class="col-md-12">
            <p style="text-align: center;">
              <picture>
              <img src="../assets/images/logo-faculdade-ateneu.png" alt="Faculdade Ateneu | Construindo Valores">
              </picture>
              <br><br>
              &#169; 2016 Faculdade Ateneu. Todos os direitos reservados.
            </p>
          </div>
        </div>
      </div>
    </footer>

    <!-- Modal Messejana -->
    <div class="modal fade" id="myModalMessejana" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <p>Unidade Messejana:</p>
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d127388.64581950518!2d-38.502645!3d-3.832665!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcb9df86346d13bc0!2sFaculdade+Ateneu!5e0!3m2!1spt-BR!2sus!4v1450900997679" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
        </div><!-- /END.modal-content -->
      </div><!-- /END.modal-dialog -->
    </div><!-- /END Modal mapa -->

    <!-- Modal Ant Bezerra -->
    <div class="modal fade" id="myModalAntBezerra" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
          <p>Unidade Ateneu Ant&#244;nio:</p>
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7962.650930929789!2d-38.597751!3d-3.739082!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7c74bba62e2a681%3A0x6954fb423c31a7d3!2sTravessa+S%C3%A3o+Vicente+de+Paula%2C+300+-+Ant%C3%B4nio+Bezerra%2C+Fortaleza+-+CE%2C+Brasil!5e0!3m2!1spt-BR!2sus!4v1450901088410" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
        </div><!-- /END.modal-content -->
      </div><!-- /END.modal-dialog -->
    </div><!-- /END Modal mapa -->

    <!-- Modal Ant Montese -->
    <div class="modal fade" id="myModalMontese" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <p>Unidade Montese:</p>
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7962.483607307586!2d-38.54098400000001!3d-3.7574600000000027!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa660578497b8c850!2sCol%C3%A9gio+Piamarta+Montese!5e0!3m2!1spt-BR!2sbr!4v1452631695024" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
        </div><!-- /END.modal-content -->
      </div><!-- /END.modal-dialog -->
    </div><!-- /END Modal mapa -->

    <!-- Modal Pec&#233;m -->
    <div class="modal fade" id="myModalPecem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
          <p>Unidade Pec&#233;m:</p>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3982.146126301108!2d-38.83791938482413!3d-3.553779497413742!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7c0dabb28d80e4d%3A0x4db0ab850a86b063!2sEducandario+Francisca+Ferreira+Martins!5e0!3m2!1spt-BR!2sus!4v1452632014146" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
        </div><!-- /END.modal-content -->
      </div><!-- /END.modal-dialog -->
    </div><!-- /END Modal mapa -->

    <!-- JS -->
    <script src="../assets/js/subir.min.js"></script>
  </body>
</html>
