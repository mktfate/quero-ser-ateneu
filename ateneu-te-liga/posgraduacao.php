﻿<!DOCTYPE html>
  <html ng-app="fate" class="ng-scope" lang="pt-BR" xmlns:fb="http://ogp.me/ns/fb#" prefix="og: http://ogp.me/ns#" class=" js flexbox webgl no-touch geolocation hashchange history websockets rgba hsla multiplebgs backgroundsize borderimage textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage applicationcache svg svgclippaths mediaqueries no-regions supports"><!--<![endif]-->
  <head>
    <title>Ateneu Te Liga</title>
    <meta name="author" content="Faculdade Ateneu">
    <meta name="robots" content="noindex">

    <!-- CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/main.css" rel="stylesheet">
  </head>
  <body>
    <!-- MAIN -->
    <div class="container">
      <form id="posgraduacao" action="http://177.22.37.162:81/webservices/cadastro_izy.asmx/Cadastro" method="POST" target="_blank">
        <input class="frmInput" type="text" size="50" name="Acesso" style="display: none;" value="6CgvsiW0D2HSsSa1foEdaQ==">
        <input class="frmInput" type="text" size="50" name="IDExterno" style="display: none;" value="">
        <input class="frmInput" type="text" size="50" name="RG" value="" style="display: none;">
        <!-- <input class="frmInput" type="text" size="50" name="Curso" value="" style="display: none;"> -->
        <input class="frmInput" type="text" size="50" name="DataNasc" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Cep" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Endereco" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Numero" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Complemento" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Bairro" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Cidade" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Estado" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Deficiencia" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="DescricaoDeficiencia" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="DataProva" value="" style="display: none;" >
        <input class="frmInput" type="text" size="50" name="Campanha" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Midia" value="Ateneu te liga" style="display: none;">
        <input class="frmInput" type="text" size="50" name="CheckInMailer" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Periodo" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Valor" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Unidade" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="TipoInscricao" value="pos_graduacao" style="display: none;">

        <div class="form-group">
        <label for="curso">Curso</label>
        <select name="Curso" class="form-control" id="curso" ng-model="curso">
          <option value="">Selecione um Curso</option>
          <option value="">---</option>
          <option value="Direito Tributário Trabalhista e Previdenciário">Direito Tributário Trabalhista e Previdenciário</option>
          <option value="Direito Militar">Direito Militar</option>
          <option value="Direito Penal">Direito Penal</option>
          <option value="Direito Previdenciário">Direito Previdenciário</option>
          <option value="Direito Processual">Direito Processual</option>
          <option value="Perícia Forense">Perícia Forense</option>
          <option value="">---</option>
          <option value="Cultura e História Afro-Brasileira">Cultura e História Afro-Brasileira</option>
          <option value="Ensino de Espanhol">Ensino de Espanhol</option>
          <option value="Ensino de Geografia">Ensino de Geografia</option>
          <option value="Gestão Escolar">Gestão Escolar</option>
          <option value="Informatica na Educação">Informatica na Educação</option>
          <option value="Língua Portuguesa e Literatura">Língua Portuguesa e Literatura</option>
          <option value="Psicopedagogia">Psicopedagogia</option>
          <option value="">---</option>
          <option value="Engenharia de Segurança do Trabalho">Engenharia de Segurança do Trabalho</option>
          <option value="">---</option>
          <option value="Gestão Ambiental">Gestão Ambiental</option>
          <option value="Gestão do Design de Moda">Gestão do Design de Moda</option>
          <option value="MBA Administração e Negócios">MBA Administração e Negócios</option>
          <option value="MBA Controladoria e Finanças">MBA Controladoria e Finanças</option>
          <option value="MBA Gestão de Recursos Humanos">MBA Gestão de Recursos Humanos</option>
          <option value="MBA Gestão Estratégica de Marketing">MBA Gestão Estratégica de Marketing</option>
          <option value="MBA Operações Logísticas">MBA Operações Logísticas</option>
          <option value="">---</option>
          <option value="Auditoria Dos Sistemas de Saúde">Auditoria Dos Sistemas de Saúde</option>
          <option value="Citologia Clínica">Citologia Clínica</option>
          <option value="Educação Física Para Grupos Especiais">Educação Física Para Grupos Especiais</option>
          <option value="Enfermagem do Trabalho">Enfermagem do Trabalho</option>
          <option value="Especialização em Assistência em Cuidados Paliativos">Especialização em Assistência em Cuidados Paliativos</option>
          <option value="Farmacologia">Farmacologia</option>
          <option value="Fisiologia e Prescrição do Exercício">Fisiologia e Prescrição do Exercício</option>
          <option value="Fisioterapia Cardiorespiratória">Fisioterapia Cardiorespiratória</option>
          <option value="Fisioterapia Dermato-Funcional">Fisioterapia Dermato-Funcional</option>
          <option value="Fisioterapia na Saúde do Trabalhador e Ergonomia">Fisioterapia na Saúde do Trabalhador e Ergonomia</option>
          <option value="Gerontologia">Gerontologia</option>
          <option value="Metodologia do Ensino da Dança">Metodologia do Ensino da Dança</option>
          <option value="Saúde da Família">Saúde da Família</option>
          <option value="Saude do Trabalhador e Ergonomia">Saude do Trabalhador e Ergonomia</option>
          <option value="Segurança Alimentar e Nutricional">Segurança Alimentar e Nutricional</option>
          <option value="Vigilância Sanitária">Vigilância Sanitária</option>
          <option value="">---</option>
          <option value="Governança de TI">Governança de TI</option>
          <option value="MBA Gerenciamento de Projetos">MBA Gerenciamento de Projetos</option>
          <option value="Segurança em Redes de Computadores">Segurança em Redes de Computadores</option>
        </select>
        </div>

        <div class="form-group">
        <label for="nome">Nome</label>
        <input class="form-control" type="text" name="Nome" id="nome" ng-model="nome" >
        </div>

        <div class="form-group">
        <label for="email">E-mail</label>
        <input class="form-control" type="text" name="Email" id="email" ng-model="email" >
        </div>

        <div class="form-group">
        <label for="telefone">Telefone</label>
        <input class="form-control" type="text" name="Telefone" id="telefone" ng-model="telefone" angular-mask="(00)00000-0000|(00)0000-0000" maxlengh="21">
        </div>

        <div class="form-group">
        <label for="cpf">CPF</label>
        <input class="form-control" type="text" name="CPF" id="cpf" ng-model="cpf" angular-mask="000.000.000-00" maxlengh="11">
        </div>

        <!-- <div class="form-group">
        <label for="tipoinscricao">Tipo de Contato</label>
        <select name="TipoInscricao" class="form-control" id="tipoinscricao" ng-model="assunto">
          <option order="1" value="" checked>Selecione uma opção</option>
          <option order="2" value="vestibular">Vestibular</option>
          <option order="3" value="transferencia">Transferência de Cursos</option>
          <option order="4" value="segunda_graduacao">Segunda Graduação</option>
          <option order="5" value="enem">ENEM</option>
          <option order="6" value="prouni">PROUNI</option>
          <option order="7" value="matricula_novatos">Matrícula Novatos</option>
          <option order="8" value="tecnico">Cursos Técnicos</option>
          <option order="9" value="pos_graduacao">Pós-Graduação</option>
          <option order="10" value="extencao">Cursos de Extensão</option>
          <option order="11" value="endereco_unidades">Endereço de Unidades</option>
          <option order="12" value="convenio_empresa">Convênio-Empresas</option>
          <option order="13" value="convenio_escola">Convênio-Escolas</option>
        </select>
        </div> -->

        <input type="submit" value="ENVIAR DADOS" class="btn btn-action">

        <div id="mensagens"></div>
      </form>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.2/angular.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.validate.js"></script>
    <script type="text/javascript" src="assets/js/angular-simple-mask.js"></script>
    <!-- <script type="text/javascript" src="assets/js/jquery.mask.js"></script> -->
    <script type="text/javascript" src="assets/js/functions.js"></script>
    <!-- <script type="text/javascript" src="assets/js/app.js"></script>     -->
  </body>
</html>
