<?php
header('Content-type: text/json');

$retorno = array();
switch($_POST['campoe_20_334'])
{
   case 1: //Antonio Bezerra
      $retorno = array(
          1 => "Administração",
          13 => "Análise e Desenvolvimento de Sistemas",
          3 => "Ciências Contábeis",
          12 => "Educação Física",
          4 => "Enfermagem",
          5 => "Engenharia Civil",
          6 => "Engenharia de Produção",
          15 => "Estética e Cosmética",
          7 => "Fisioterapia",
          17 => "Gestão de Turismo",
          16 => "Gestão Financeira",
          18 => "Logística",
          12 => "Pedagogia",
          20 => "Processos Gerenciais",
          9 => "Psicologia",
          21 => "Recursos Humanos",
          22 => "Redes de Computadores",
          23 => "Secretariado",
          10 => "Serviço Social"
      );
      break;
   case 2: //Messejana
      $retorno = array(
          1=>  "Administração",
          13 => "Análise e Desenvolvimento de Sistemas",
          2 => "Arquitetura e Urbanismo",
          3 => "Ciências Contábeis",
          14 => "Design de Moda",
          12 => "Educação Física",
          4 => "Enfermagem",
          5 => "Engenharia Civil",
          7 => "Fisioterapia",
          17 => "Gestão de Turismo",
          16 => "Gestão Financeira",
          18 => "Logística",
          19 => "Marketing",
          8 => "Nutrição",
          12 => "Pedagogia",
          20 => "Processos Gerenciais",
          21 => "Recursos Humanos",
          22 => "Redes de Computadores",
          23 => "Secretariado",
          10 => "Serviço Social"
      );
      break;
   case 3: //Pecem
      $retorno = array(
          1  => "Administração",
          13 => "Análise e Desenvolvimento de Sistemas",
          18 => "Logística",
          21 => "Recursos Humanos",
          22 => "Redes de Computadores",
          26 => "Comércio Exterior",
          27 => "Gestão Portuária"
      );
      break;
    case 4: //Montese
      $retorno = array(
          1 => "Administração",
          13 => "Análise e Desenvolvimento de Sistemas",
          3 => "Ciências Contábeis",
          18 => "Logística",
          21 => "Recursos Humanos",
          22 => "Redes de Computadores"
      );
      break;
}

echo json_encode($retorno);
?>
