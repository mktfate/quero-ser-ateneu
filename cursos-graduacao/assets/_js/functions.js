$(document).ready(function(){

    $("#campoe_20").change(function(){
        $.ajax({
            type: "POST",
            url: "../assets/inc/banco.php",
            data: {campoe_20_334: $("#campoe_20").val()},
            dataType: "json",
            success: function(json){
                var options = "";
                $.each(json, function(key, value){
                    //options += '<option value="' + key + '">' + value + '</option>';
                    options += '<option style="" value="' + key + '" title="' + value + '" visible="visible">' + value + '</option>';
                });
                $("#campoe_18").html(options);
            }
        });
    });

    $(".campoe_20").change(function(){
        $.ajax({
            type: "POST",
            url: "assets/inc/banco.php",
            data: {campoe_20_334: $("#campoe_20").val()},
            dataType: "json",
            success: function(json){
                var options = "";
                $.each(json, function(key, value){
                    //options += '<option value="' + key + '">' + value + '</option>';
                    options += '<option style="" value="' + key + '" title="' + value + '" visible="visible">' + value + '</option>';
                });
                $("#campoe_18").html(options);
            }
        });
    });

    $('#easyform_1eFeDxueq9lUow9Ce6844425-').validate({
        //regras e mensagens para os campos
        rules: {
            campoe_19_333: {required: true},                 //forma de ingresso  
            fname_331:     { required: true, minlength: 5 }, // Nome
            email_332:     { required: true, email: true },  //email
            campoe_15_336: { required: true},                //telefone
            campoe_20_334: {required: true},                 //Unidade
            campoe_18_335: {required: true},                 //Curso
            campoe_21_344: {required: true},                 //Curso Atual
            campoe_22_345: {required: true},                 // Semestre
            campoe_23_346: {required: true},                 //Instituição
            campoe_26_347: {required: true}                  //Primeiro Curso
        },
        messages: {
            campoe_19_333: {required: "Selecione a forma de ingresso"},                                                      //forma de ingresso 
            fname_331: { required: 'Por favor, digite seu nome!', minlength: 'Por favor, digite seu nome!' },                // Nome
            email_332: { required: 'Por favor, informe o seu email!', email: 'por favor, informe um email v&aacute;lido!' }, //email
            campoe_15_336: { required: 'Por favor, informe o seu telefone!'},                                                //telefone
            campoe_20_334: {required: 'Por favor, selecione uma Unidade Ateneu!'},                                           //Unidade
            campoe_18_335: {required: 'Por favor, selecione o seu curso!'},                                                  //Curso
            campoe_21_344: {required: 'Por favor, informe seu curso atual'},                                                 //Curso Atual
            campoe_22_345: {required: 'Por favor, informe seu semestre atual'},                                              //Semestre
            campoe_23_346: {required: 'Por favor, informe sua instituição atual'},                                           //Instituição
            campoe_26_347: {required: 'Por favor, informe sua primeira graduação'}                                           //Primeiro Curso
        },

        //Monta a mensagem em uma caixa separada
        errorContainer:"#mensagens",
        errorLabelContainer:"#mensagens ul",
        wrapper:"span"
    });

    $("#campoe_15_336").mask("(99)99999-9999");

});
