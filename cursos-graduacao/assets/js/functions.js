$(document).ready(function(){

    $('#webservice').validate({
        rules: {
            Curso:          { required: true},                                                //Curso
            TipoInscricao:  { required: true},                                                //TipoInscricao
            Unidade:        { required: true},                                                //Unidade
            Nome:           { required: true, minlength: 6 },                                 //Nome
            Email:          { required: true, email: true },                                  //Email
            Telefone:       { required: true, minlength: 13, maxlength: 14},                  //Telefone
            cpf:            { required: true, minlength: 14, maxlength: 14},                  //CPF
            TipoInscricao:  { required: true}                                                 //Tipo de Iscrição
        },

        messages: {
            Curso:          { required: 'Por favor, selecione um curso.'},
            TipoInscricao:  { required: 'Por favor, escolha uma forma de ingresso.'},
            Unidade:        { required: 'Por favor, selecione uma unidade.'},
            Nome:           { required: 'Por favor, digite seu primeiro e último nome.', minlength: 'Seu nome deve conter mais de 6 letras.'},
            Email:          { required: 'Por favor, informe o seu email.', email: 'Por favor, informe um email v&aacute;lido.'},
            Telefone:       { required: 'Por favor, informe o seu telefone.', minlength: 'seu telefone deve conter o mínimo de 10 digitos', maxlength: 'seu telefone deve conter o máximo de 11 digitos'},                                                                   //telefone
            cpf:            { required: 'Por favor, informe o seu CPF.', minlength: 'seu CPF deve conter 11 digitos', maxlength: 'seu CPF deve conter 11 digitos'},                                                                         //CPF
            TipoInscricao:  { required: 'Por favor, selecione o tipo de contato.'}
        },

        // Monta a mensagem em uma caixa separada
        errorContainer:"#mensagens",
        errorLabelContainer:"#mensagens ul",
        wrapper:"span"
    });

    $('#e-goi').validate({
        rules: {
            // Curso:          { required: true},                                                //Curso
            // Unidade:        { required: true},                                                //Unidade
            fname_469:         { required: true, minlength: 6 },                                 //Nome
            email_470:         { required: true, email: true },                                  //Email
            // Telefone:       { required: true, minlength: 13, maxlength: 14},                  //Telefone
            // cpf:            { required: true, minlength: 14, maxlength: 14},                  //CPF
            // TipoInscricao:  { required: true}                                                 //Tipo de Iscrição
        },

        messages: {
            // Curso:          { required: 'Por favor, selecione um curso.'},
            // Unidade:        { required: 'Por favor, selecione uma unidade.'},
            fname_469:         { required: 'Por favor, digite seu primeiro e último nome.', minlength: 'Seu nome deve conter mais de 6 letras.'},
            email_470:         { required: 'Por favor, informe o seu email.', email: 'Por favor, informe um email v&aacute;lido.'},
            // Telefone:       { required: 'Por favor, informe o seu telefone.', minlength: 'seu telefone deve conter o mínimo de 10 digitos', maxlength: 'seu telefone deve conter o máximo de 11 digitos'},                                                                   //telefone
            // cpf:            { required: 'Por favor, informe o seu CPF.', minlength: 'seu CPF deve conter 11 digitos', maxlength: 'seu CPF deve conter 11 digitos'},                                                                         //CPF
            // TipoInscricao:  { required: 'Por favor, selecione o tipo de contato.'}
        },

        // Monta a mensagem em uma caixa separada
        errorContainer:"#mensagens",
        errorLabelContainer:"#mensagens ul",
        wrapper:"span"
    });

});

angular.module('myApp', ['ngCpfCnpj', 'angularMask']);
