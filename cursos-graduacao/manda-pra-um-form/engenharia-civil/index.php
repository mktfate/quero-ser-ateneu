﻿<!DOCTYPE html>
<!--[if lt IE 7 ]><html
lang="pt-BR" prefix="og: http://ogp.me/ns#" class="ie6"> <![endif]-->
<!--[if IE 7 ]><html
lang="pt-BR" prefix="og: http://ogp.me/ns#" class="ie7"> <![endif]-->
<!--[if IE 8 ]><html
lang="pt-BR" prefix="og: http://ogp.me/ns#" class="ie8"> <![endif]-->
<!--[if IE 9 ]><html
lang="pt-BR" prefix="og: http://ogp.me/ns#" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="pt-BR" ng-app="myApp" xmlns:fb="http://ogp.me/ns/fb#" prefix="og: http://ogp.me/ns#" class=" js flexbox webgl no-touch geolocation hashchange history websockets rgba hsla multiplebgs backgroundsize borderimage textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage applicationcache svg svgclippaths mediaqueries no-regions supports"><!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Curso Gradua&ccedil;&atilde;o Engenharia Civil | Faculdade Ateneu</title>
    <meta name="description" content="Fa&ccedil;a o Curso de Gradua&ccedil;&atilde;o em Engenharia Civil na Ateneu. Estude Engenharia Civil com a nota do ENEM, Vestibular, Transfer&#234;ncia ou Segunda Gradua&#231;&#227;o.">
    <meta name="keywords" content="curso, engenharia Civil, engenharia, constru&ccedil;&atilde;o civil, engenheiro civil, vestibular, gradua&#231;&#227;o, faculdade ateneu, enem, resultado enem, segunda gradua&#231;&#227;o, transfer&#234;ncia de curso, fies, financiamento sem juros">
    <meta name="author" content="Faculdade Ateneu">
    <meta name="robots" content="noodp,noydir">
    <link rel="shortcut icon" href="../assets/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="../images/assets/favicon.ico" type="image/x-icon">
    <link href="http://queroserateneu.com.br/cursos-graduacao/engenharia-civil/post-engcivil.jpg" rel="image_src"/>

    <meta property="og:locale" content="pt_BR">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Gradua&#231;&#227;o Engenharia Civil | Faculdade Ateneu">
    <meta property="og:description" content="Estude na Ateneu pagando menos e ganhe diversos cursos de extens&#227;o.">
    <meta property="og:url" content="http://queroserateneu.com.br/cursos-graduacao/enganharia-civil/?<?php $string = basename($_SERVER['QUERY_STRING']); echo $string ?>">
    <meta property="og:site_name" content="Faculdade Ateneu">
    <meta property="og:image" content="http://queroserateneu.com.br/cursos-graduacao/engenharia-civil/post-engcivil.jpg">
    <meta property="og:image:width" content="800">
    <meta property="og:image:height" content="419">

    <!-- Bootstrap core CSS -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/css/main.css" rel="stylesheet">
    <link href="../assets/css/sweetalert2.min.css" rel="stylesheet">

    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../assets/js/angular.min.js"></script>
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/jquery.validate.js"></script>
    <script src="../assets/js/angular-simple-mask.js"></script>
    <script src="../assets/js/functions.js"></script>
    <!-- ALERT -->
    <script src="../assets/js/sweetalert2.min.js"></script>
    <!-- CPF -->
    <script src="../assets/js/cpf.min.js"></script>
    <script src="../assets/js/ngCpfCnpj.js"></script>

  </head>
  <body style="background-image: url(../assets/images/bg-eng-civil.jpg);">
    <!-- MAIN -->
    <div class="container">
      <div class="table">
        <div class="table-cell">
          <!-- Logo -->
          <div class="col-md-12 logo">
            <picture>
              <img src="../assets/images/logo-faculdade-ateneu-montese-white.png" alt="Faculdade Ateneu | Construindo Valores">
            </picture>
          </div>

          <!-- Destaque -->
          <div class="col-md-12 col-sm-12 col-xs-12 destaque">
            <div class="col-md-4 col-sm-4 hidden-xs imagem">
              <picture>
                <img src="../assets/images/modelo-campanha-cursos.png" alt="Curso de Gradua&#231;&#227;o em Engenharia Civil Faculdade Ateneu" title="Curso de Gradua&#231;&#227;o em Engenharia Civil Faculdade Ateneu">
              </picture>
            </div>

            <div class="col-md-8 col-sm-8 col-xs-12 texto">
                <h4 class="yellow-color">Inscri&ccedil;&otilde;es Gratuitas Curso de Gradua&ccedil;&atilde;o Ateneu</h4>
                <h1>
                  <span>CURSO DE GRADUA&#199;&#195;O</span><br>
                  ENGENHARIA CIVIL
                </h1>
                <h2>Dose dupla Ateneu. S&#243; na Faculdade Ateneu voc&#234; faz o Curso de Gradua&#231;&#227;o em Engenharia Civil <span class="yellow-color">pagando menos</span> e ganha diversos <span class="label">cursos de extens&#227;o.</span></h2>
            </div>
          </div>
          <!-- Close Destaque -->


          <!-- Open Oferta -->
          <div class="col-md-12 col-sm-12 col-xs-12 desconto">
            <!-- Infos -->
            <div class="col-md-12 col-sm-12 col-xs-12 infos">
              <div class="col-md-12 col-sm-12 col-xs-12">

              <div class="col-md-3 col-sm-6 col-xs-6 info-bloco">
                <div class="col-md-3 col-sm-4 col-xs-4 icon">
                  <div class="table">
                    <div class="table-cell">
                      <i class="glyphicon glyphicon-star"></i>
                    </div>
                  </div>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-8 content">
                  <div class="table">
                    <div class="table-cell">
                      <span class="blue-color">90% de desconto</span><br>na matr&iacute;cula<sup>1</sup>
                    </div>
                  </div>
                </div>
              </div><div class="col-md-3 col-sm-6 col-xs-6 info-bloco">
                <div class="col-md-3 col-sm-4 col-xs-4 icon">
                  <div class="table">
                    <div class="table-cell">
                      <i class="glyphicon glyphicon-education"></i>
                    </div>
                  </div>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-8 content">
                  <div class="table">
                    <div class="table-cell">
                      <span class="blue-color">At&#233; 30% de desconto</span><br>nas mensalidades<sup>2</sup>
                    </div>
                  </div>
                </div>
              </div><div class="col-md-3 col-sm-6 col-xs-6 info-bloco">
                <div class="col-md-3 col-sm-4 col-xs-4 icon">
                  <div class="table">
                    <div class="table-cell">
                      <i class="glyphicon glyphicon-usd"></i>
                    </div>
                  </div>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-8 content">
                  <div class="table">
                    <div class="table-cell">
                      <span class="blue-color">Isento da taxa</span><br>de inscri&ccedil;&atilde;o
                    </div>
                  </div>
                </div>
              </div><div class="col-md-3 col-sm-6 col-xs-6 info-bloco">
                <div class="col-md-3 col-sm-4 col-xs-4 icon">
                  <div class="table">
                    <div class="table-cell">
                      <i class="glyphicon glyphicon-ok"></i>
                    </div>
                  </div>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-8 content">
                  <div class="table">
                    <div class="table-cell">
                      <span class="blue-color">Curso de Extens&#227;o</span><br>gr&#225;tis<sup>3</sup>
                    </div>
                  </div>
                </div>
              </div>

            </div>
            </div>
            <!-- /END Infos -->
          </div>
          <!-- /END Oferta -->

          <!-- Open Formul&#225;rio -->
          <div class="col-md-12 col-sm-12 col-xs-12 formulario">
            <form name="myForm" id="webservice" action="http://177.22.37.162:81/webservices/cadastro_izy.asmx/Cadastro" method="POST" target="action">
                <input type="text" size="50" name="Acesso" style="display: none;" value="6CgvsiW0D2HSsSa1foEdaQ==">
                <input type="text" size="50" name="IDExterno" style="display: none;" value="">
                <input type="text" size="50" name="RG" value="" style="display: none;">
                <input type="text" size="50" name="DataNasc" value="" style="display: none;">
                <input type="text" size="50" name="Cep" value="" style="display: none;">
                <input type="text" size="50" name="Endereco" value="" style="display: none;">
                <input type="text" size="50" name="Numero" value="" style="display: none;">
                <input type="text" size="50" name="Complemento" value="" style="display: none;">
                <input type="text" size="50" name="Bairro" value="" style="display: none;">
                <input type="text" size="50" name="Cidade" value="" style="display: none;">
                <input type="text" size="50" name="Estado" value="" style="display: none;">
                <input type="text" size="50" name="Deficiencia" value="" style="display: none;">
                <input type="text" size="50" name="DescricaoDeficiencia" value="" style="display: none;">
                <input type="text" size="50" name="DataProva" value="" style="display: none;" >
                <input type="text" size="50" name="CheckInMailer" value="" style="display: none;">
                <input type="text" size="50" name="Periodo" value="" style="display: none;">
                <input type="text" size="50" name="Valor" value="" style="display: none;">
                <input type="text" size="50" name="Curso" value="Engenharia Civil" style="display: none;">

                <?php
                  // Pegar Query Strings
                  $string = basename($_SERVER['QUERY_STRING']);
                  $dados = explode('&',$string);
                  $total = count($dados);
                  $array = '';

                  $campanha = ltrim(strstr($dados[0], '='), '=');
                  $midia = ltrim(strstr($dados[1], '='), '=');
                  $consultor = ltrim(strstr($dados[2], '='), '=');

                  //Campanha - Hidden
                  echo '<input type="text" name="Campanha" value="'.$campanha.'" style="display:none;">';
                  echo '<input type="text" name="Midia" value="'.$midia.'" style="display:none;">';
                ?>

              <!-- Radios Processos -->
              <div class="col-md-12 list-radios">
                <div class="col-md-12"><h4>Escolha como deseja ingressar no Curso de Gradua&#231;&#227;o em Engenharia Civil da Faculdade Ateneu:</h4></div>
                <div class="col-md-3 ">
                  <input id="vestibular" type="radio" value="vestibular" title="Vestibular" name="TipoInscricao" class="radio" ng-model="tipoinscricao">
                  <label for="vestibular">Vestibular</label>
                </div>
                <div class="col-md-3">
                  <input id="enem" type="radio" value="enem" title="Enem" name="TipoInscricao" class="radio" ng-model="tipoinscricao">
                  <label for="enem">Nota do Enem</label>
                </div>
                <div class="col-md-3">
                  <input id="transferencia" type="radio" value="transferencia" title="Transfer&#234;ncia" name="TipoInscricao" class="radio" ng-model="tipoinscricao">
                  <label for="transferencia">Transfer&#234;ncia</label>
                </div>
                <div class="col-md-3">
                  <input id="segunda_graduacao" type="radio" value="segunda_graduacao" title="2&#170; Gradua&#231;&#227;o" name="TipoInscricao" class="radio" ng-model="tipoinscricao">
                  <label for="segunda_graduacao">2&#170; Gradua&#231;&#227;o</label>
                </div>
              </div>
              <!-- /END Radios Processos -->

              <!-- Dados Pessoais -->
              <div class="col-md-12">
                <div class="form-group col-md-6">
                <input class="form-control" type="text" name="Nome" id="nome" ng-model="nome" placeholder="Nome" >
                </div>

                <div class="form-group col-md-3">
                <input class="form-control" type="text" name="Email" id="email" ng-model="email" placeholder="E-mail" >
                </div>

                <div class="form-group col-md-3">
                <input class="form-control" type="text" name="Telefone" id="telefone" ng-model="telefone" angular-mask="(00)00000-0000" placeholder="Telefone">
                </div>
              </div>
              <!-- /END Dados Pessoais -->

              <div class="col-md-12">
                <!-- Unidade -->
                <div class="form-group col-md-6">
                  <select class="form-control" name="Unidade" id="unidade" ng-model="unidade">
                    <option selected="selected" value="" title="Unidade Ateneu"> Escolha uma Unidade</option>
                    <option value="Antonio Bezerra" title="Antonio Bezerra">Ant&#244;nio Bezerra</option>
                    <!-- <option value="Aldeota" title="Aldeota">Aldeota</option> -->
                    <option value="Messejana" title="Messejana">Messejana</option>
                    <!-- <option value="Pecem" title="Pecem">Pec&#233;m</option> -->
                  </select>
                </div>

                <!-- CPF -->
                <div class="form-group col-md-6">
                  <input class="form-control" type="text" name="cpf" id="cpf" ng-cpf ng-model="cpf" angular-mask="000.000.000-00" placeholder="CPF">
                  <label class="error" ng-show="myForm.cpf.$invalid">Por favor, informe um CPF Válido!</label>
                  <label ng-hide="myForm.cpf.$valid"></label>
                </div>
              </div>

              <div id="mensagens"></div>

              <div class="col-md-12">
                <div class="form-group col-md-12">
                  <input ng-disabled="!nome || !email || !telefone || !cpf || !unidade  || !tipoinscricao" type="submit" id="enviar" value="Inscreva-se Agora!" class="btn btn-default" onclick="mandaForm();">
                  <small>*Preencha todos os campos para ativar o botão de inscrição.</small>
                </div>
              </div>
            </form>

            <form class="hidden" id="e-goi" method="post" enctype="multipart/form-data" action="http://mkt.queroserateneu.com.br/w/1eTeDxuefkyg4vHPle296f-3-d" target="action2">
              <input type="hidden" name="lista" value="1">
              <input type="hidden" name="cliente" value="138749">
              <input type="hidden" name="lang" id="lang_id" value="br">
              <input type="hidden" name="formid" id="formid" value="53">
              <input name="fname_469" id="fname_469" value="{{nome}}" type="text">
              <input name="email_470" id="email_470" value="{{email}}" type="email" easyvalidation="false">
              <input name="campoe_15_474" id="campoe_15_474" value="{{telefone}}" type="text">
              <input type="text" name="campoe_34_490" id="campoe_34_490" value="{{cpf}}">
              <input type="text" name="campoe_44_487" id="campoe_44_487" value="{{tipoinscricao}}">
              <input type="text" name="campoe_42_488" id="campoe_42_488" value="{{unidade}}">
              <select useoutrolabel="Outro (qual?)" data-theme="grey" name="campoe_18_473" id="campoe_18">
                <option selected="selected" value="5" title="Bacharelado em Engenharia Civil">
                  Bacharelado em Engenharia Civil
                </option>
              </select>

              <?php
                // Pegar URL
                $urlfull = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                $url = explode('/', $urlfull);
                $urlfinal = "$url[0]/$url[1]/$url[2]";

                // Pegar Query Strings
                $string = basename($_SERVER['QUERY_STRING']);
                $dados = explode('&',$string);
                $total = count($dados);
                $array = '';

                $campanha = ltrim(strstr($dados[0], '='), '=');
                $midia = ltrim(strstr($dados[1], '='), '=');
                $consultor = ltrim(strstr($dados[2], '='), '=');

                //Campanha - Hidden
                echo '<input type="text" name="campoe_16_475" id="campoe_16_475" value="'.$campanha.'">';
                echo '<input type="text" name="campoe_43_489" id="campoe_43_489" value="'.$midia.'">';
                echo '<input type="text" name="campoe_30_477" id="campoe_30_477" value="'.$consultor.'">';
                echo '<input type="text" name="campoe_17_476" id="campoe_17_476" value="'.$urlfinal.'">';
              ?>
            </form>

            <iframe class="hidden" src="action.php" name="action" frameborder="0"></iframe>
            <iframe class="hidden" src="action2.php" name="action2" frameborder="0"></iframe>

          </div>
          <!-- /END Formul&#225;rio -->

          <!-- Open Oferta -->
          <div class="col-md-12 col-sm-12 col-xs-12 desconto">
            <h3>Com o <strong>Curso de Gradua&#231;&#227;o em Engenharia Civil da Faculdade Ateneu</strong>, voc&#234; tem al&#233;m de um curr&#237;culo moderno e professores de qualidade, diversos cursos de extens&#227;o. &#201; a sua oportunidade de se especializar em diversos campos e ter um mercado de trabalho muito mais favor&#225;vel quando sair com diploma na m&#227;o. Entendeu?</h3>
          </div>
          <!-- /END Oferta -->

          <!-- Infos -->
          <div class="col-md-12 col-sm-12 col-xs-12 infos">
            <div class="col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1 col-xs-10 col-xs-offset-1 info-bloco">
              <div class="col-md-3 col-sm-3 col-xs-3 icon">
                <div class="table">
                  <div class="table-cell">
                    <i class="glyphicon glyphicon-file"></i>
                  </div>
                </div>
              </div>
              <div class="col-md-9 col-sm-9 col-xs-9 content">
                <div class="table">
                  <div class="table-cell">
                    Sobre o Curso | <a href="http://fate.edu.br/curso/engenharia-civil/" class="bt" target="_blank" ><i class="glyphicon glyphicon-chevron-right"></i> Ver Mais</a> <br>
                    Edital 2016.2 | <a href="http://fate.edu.br/documentos/editais/vestibular/faculdade-ateneu-edital-vestibular-tradicional-20162.pdf" class="bt" target="_blank" ><i class="glyphicon glyphicon-chevron-right"></i> Ver Edital</a> <br>
                    Manual do Aluno | <a href="http://queroserateneu.com.br/assets/inc/doc/manual-do-aluno-fate-2016.pdf" class="bt" target="_blank" ><i class="glyphicon glyphicon-chevron-right"></i> Ver Manual</a>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1 col-xs-10 col-xs-offset-1 info-bloco">
              <div class="col-md-3 col-sm-3 col-xs-3 icon">
                <div class="table">
                  <div class="table-cell">
                    <i class="glyphicon glyphicon-map-marker"></i>
                  </div>
                </div>
              </div>
              <div class="col-md-9 col-sm-9 col-xs-9 content">
                <div class="table">
                  <div class="table-cell">
                    <h3 class="yellow-color">Unidade Ateneu</h3>
                    Messejana: <a href="#" class="bt" data-toggle="modal" data-target="#myModalMessejana"><i class="glyphicon glyphicon-chevron-right"></i> Ver Mapa</a><br>
                    Ant. Bezerra: <a href="#" class="bt" data-toggle="modal" data-target="#myModalAntBezerra"><i class="glyphicon glyphicon-chevron-right"></i> Ver Mapa</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- /END Infos -->

           <!-- Footer -->
          <footer>
            <div class="col-md-12 col-sm-12 col-xs-12 copy">
              <p>Com o <a href="http://fate.edu.br/curso/engenharia-civil/" target="_blank"><strong>CURSO DE GRADUA&Ccedil;&Atilde;O EM ENGENHARIA CIVIL</strong></a> da Faculdade Ateneu, voc&ecirc; ser&aacute; o pr&oacute;ximo construtor e desenvolvedor de projetos de edifica&ccedil;&atilde;o, aportando com sua criatividade e aplicando seus conhecimentos dentro de uma das ind&uacute;strias mais rent&aacute;veis do mundo. Al&eacute;m de contar com um curr&iacute;culo moderno e professores de qualidade, voc&ecirc; tamb&eacute;m ganhar&aacute; diversos cursos de extens&atilde;o.Entendeu?
              </p>
              <br>
              <p><sup>1</sup> Desconto exclusivo de 90% na matr&#237;cula de novatos no <a href="http://fate.edu.br/curso/engenharia-civil/" target="_blank"><strong>Curso de Gradua&#231;&#227;o em Engenharia Civil</strong></a> da Faculdade Ateneu para o semestre de 2016.2, efetuadas at&#233; o dia 30 de junho de 2016.</p>
              <br>
              <p><sup>2</sup> Desconto Especial de 30% nas mensalidades do <a href="http://fate.edu.br/curso/engenharia-civil/" target="_blank"><strong>Curso de Gradua&#231;&#227;o em Engenharia Civil</strong></a> da Faculdade Ateneu, referente as matr&iacute;culas efetuadas via processo de Transfer&ecirc;ncia de Curso ou Segunda Gradua&ccedil;&atilde;o  at&eacute; o dia 30 de junho de 2016.</p>
              <br>
              <p><sup>2</sup> Desconto Especial de 20% nas mensalidades do <a href="http://fate.edu.br/curso/engenharia-civil/" target="_blank"><strong>Curso de Gradua&#231;&#227;o em Engenharia Civil</strong></a> da Faculdade Ateneu no semestre de 2016.2, referente a matr&#237;culas de novatos efetuadas at&#233; o dia 30 de junho de 2016 por meio do resultado do ENEM.</p>
              <br>
              <p><sup>2</sup> Desconto Especial de 30% no primeiro semestre do <a href="http://fate.edu.br/curso/engenharia-civil/" target="_blank"><strong>Curso de Gradua&#231;&#227;o em Engenharia Civil</strong></a> da Faculdade Ateneu no semestre de 2016.2, referente a matr&#237;culas de novatos efetuadas por meio do processo de vestibular at&#233; o dia 30 de junho de 2016.</p>
              <br>
              <p><sup>3</sup> Matr&#237;cule-se at&#233; 30 de junho de 2016 e Ganhe 1 Curso de Extens&#227;o EAD totalmente gr&#225;tis.</p>
              <br>
              <p>Promo&#231;&#227;o n&#227;o cumulativa com outras promo&#231;&#245;es e bolsas da Faculdade Ateneu.</p>
              <br>
              <p>&#169; 2016 Faculdade Ateneu. Todos os direitos reservados.</p>
            </div>
          </footer>


        </div><!-- /END Table-Cell -->
      </div><!-- /END Table -->
    </div><!-- /END Container -->

    <!-- Modal Messejana -->
    <div class="modal fade" id="myModalMessejana" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <h4>Unidade Messejana:</h4>
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d127388.64581950518!2d-38.502645!3d-3.832665!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcb9df86346d13bc0!2sFaculdade+Ateneu!5e0!3m2!1spt-BR!2sus!4v1450900997679" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
        </div><!-- /END.modal-content -->
      </div><!-- /END.modal-dialog -->
    </div><!-- /END Modal mapa -->

     <!-- Modal Mapa -->
    <div class="modal fade" id="myModalAntBezerra" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
          <h4>Unidade Ant&#244;nio Bezerra:</h4>
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7962.650930929789!2d-38.597751!3d-3.739082!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7c74bba62e2a681%3A0x6954fb423c31a7d3!2sTravessa+S%C3%A3o+Vicente+de+Paula%2C+300+-+Ant%C3%B4nio+Bezerra%2C+Fortaleza+-+CE%2C+Brasil!5e0!3m2!1spt-BR!2sus!4v1450901088410" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
        </div><!-- /END.modal-content -->
      </div><!-- /END.modal-dialog -->
    </div><!-- /END Modal mapa -->
  </body>
</html>
