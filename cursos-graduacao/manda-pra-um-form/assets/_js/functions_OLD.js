$(document).ready(function(){

    			/*Adicionar mascara ao campo TELEFONE*/
    			$("#campoe_15_336").mask("(99)99999-9999");

                $('#easyform_1eFeDxue3AQ8fnTxze40dafb83').validate({

                  //regras e mensagens para os campos
                  rules:{
                    fname_331:    {required: true, minlength: 5},
                    email_332:    {required: true, email: true},
                    campoe_15_336: {required: true},
                    campoe_19_333: {required: true},
                    campoe_18_335: {required: true}
                  },
                  messages: {
                    fname_331:     {required: 'Preencha o campo nome!', minlength: 'Informe seu Nome e Sobrenome!'},
                    email_332:     {required: 'Informe o seu E-mail', email: 'Informe E-mail válido!'},
                    campoe_15_336:  {required: 'Informe Seu Telefone!'},
                    campoe_19_333:  {required: 'Por favor selecione uma opção!'},
                    campoe_18_335:  {required: 'Selecione o Curso de sua preferência!'}
                  },

                  //Mostrar as mensagens
                  //errorContainer: "#mensagens",
                  //errorLabelContainer: "ul",
                  wrapper: "small"
                });
    			// Campo Selecionar Curso
    			//campoe_19_333

    		});