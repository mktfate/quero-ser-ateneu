﻿<!DOCTYPE html>
<!--[if lt IE 7 ]><html
lang="pt-BR" prefix="og: http://ogp.me/ns#" class="ie6"> <![endif]-->
<!--[if IE 7 ]><html
lang="pt-BR" prefix="og: http://ogp.me/ns#" class="ie7"> <![endif]-->
<!--[if IE 8 ]><html
lang="pt-BR" prefix="og: http://ogp.me/ns#" class="ie8"> <![endif]-->
<!--[if IE 9 ]><html
lang="pt-BR" prefix="og: http://ogp.me/ns#" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="pt-BR" ng-app="myApp" xmlns:fb="http://ogp.me/ns/fb#" prefix="og: http://ogp.me/ns#" class=" js flexbox webgl no-touch geolocation hashchange history websockets rgba hsla multiplebgs backgroundsize borderimage textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage applicationcache svg svgclippaths mediaqueries no-regions supports"><!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Curso Gradua&ccedil;&atilde;o Arquitetura e Urbanismo | Faculdade Ateneu</title>
    <meta name="description" content="Fa&ccedil;a o Curso de Gradua&ccedil;&atilde;o Arquitetura e Urbanismo na Ateneu. Estude Arquitetura e Urbanismo com a sua nota do ENEM, Vestibular, Transfer&#234;ncia ou Segunda Gradua&#231;&#227;o.">
    <meta name="keywords" content="arquitetura e urbanismo, arquitetura, urbanismo, vestibular, gradua&#231;&#227;o, arquiteto, faculdade ateneu, enem, resultado enem, segunda gradua&#231;&#227;o, transfer&#234;ncia de curso, fies, financiamento sem juros">
    <meta name="author" content="Faculdade Ateneu">
    <meta name="robots" content="noodp,noydir">
    <link rel="shortcut icon" href="../assets/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="../images/assets/favicon.ico" type="image/x-icon">
    <link href="http://queroserateneu.com.br/cursos-graduacao/arquitetura-e-urbanismo/post-arquitetura.jpg" rel="image_src"/>

    <meta property="og:locale" content="pt_BR">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Gradua&#231;&#227;o Arquitetura e Urbanismo Ateneu">
    <meta property="og:description" content="Estude na Ateneu pagando menos e ganhe diversos cursos de extens&#227;o.">
    <meta property="og:url" content="http://queroserateneu.com.br/cursos-graduacao/arquitetura-e-urbanismo/?<?php $string = basename($_SERVER['QUERY_STRING']); echo $string ?>">
    <meta property="og:site_name" content="Faculdade Ateneu">
    <meta property="og:image" content="http://queroserateneu.com.br/cursos-graduacao/arquitetura-e-urbanismo/post-arquitetura.jpg">
    <meta property="og:image:width" content="800">
    <meta property="og:image:height" content="419">

    <!-- Bootstrap core CSS -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/css/main.css" rel="stylesheet">
    <link href="../assets/css/sweetalert2.min.css" rel="stylesheet">

    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../assets/js/angular.min.js"></script>
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/jquery.validate.js"></script>
    <script src="../assets/js/angular-simple-mask.js"></script>
    <script src="../assets/js/functions.js"></script>
    <script src="../assets/js/app.js"></script>
    <!-- ALERT -->
    <script src="../assets/js/sweetalert2.min.js"></script>
    <!-- CPF -->
    <script src="../assets/js/cpf.min.js"></script>
    <script src="../assets/js/ngCpfCnpj.js"></script>

  </head>
  <body style="background-image: url(../assets/images/bg-arquitetura.jpg);">
    <!-- MAIN -->
    <div class="container">
      <div class="table">
        <div class="table-cell">
          <!-- Logo -->
          <div class="col-md-12 logo">
            <picture>
              <img src="../assets/images/logo-faculdade-ateneu-montese-white.png" alt="Faculdade Ateneu | Construindo Valores">
            </picture>
          </div>

          <!-- Destaque -->
          <div class="col-md-12 col-sm-12 col-xs-12 destaque">
            <div class="col-md-4 col-sm-4 hidden-xs imagem">
              <picture>
                <img src="../assets/images/modelo-campanha-cursos.png" alt="Curso de Gradua&#231;&#227;o em Arquitetura e Urbanismo Faculdade Ateneu" title="Curso de Gradua&#231;&#227;o em Arquitetura e Urbanismo Faculdade Ateneu">
              </picture>
            </div>

            <div class="col-md-8 col-sm-8 col-xs-12 texto">
                <h4 class="yellow-color">Inscri&ccedil;&otilde;es Gratuitas Curso de Gradua&ccedil;&atilde;o Ateneu</h4>
                <h1>
                  <span>CURSO DE GRADUA&#199;&#195;O</span><br>
                  ARQUITETURA E URBANISMO
                </h1>

          <!--  <h2>Dose dupla Ateneu. S&#243; na Faculdade Ateneu voc&#234; faz o Curso de Gradua&#231;&#227;o em Arquitetura e Urbanismo <span class="yellow-color">pagando menos</span> e ganha diversos <span class="label">cursos de extens&#227;o.</span></h2> -->
                <h2>Sua carreira focada no <span class="label">SUCESSO PROFISSIONAL.</span></h2>

                <!-- Selo Desktop -->
                <div class="col-md-12 col-sm-12 col-xs-12 selos">
                 <!--  <div class="col-md-5 hidden-sm hidden-xs texto-selos">
                     Entre as Melhores<br>
                     do Nordeste
                  </div>
                  <div class="col-md-5 col-sm-12 col-xs-12 visible-sm visible-xs texto-selos">
                     Entre as Melhores dos Nordeste
                  </div>
                  <div class="col-md-4 col-md-offset-0 col-sm-8 col-sm-offset-2 col-xs-12 col-xs-offset-0 imagem">
                    <picture>
                      <img src="../assets/images/selos.png" alt="Entre as Melhores do Nordeste">
                    </picture>
                  </div>
                </div> -->
                <!-- /END Selo Desktop -->
            </div>
          </div>
        </div>
          <!-- Close Destaque -->


          <!-- Open Oferta -->
          <div class="col-md-12 col-sm-12 col-xs-12 desconto">
            <!-- Infos -->
            <div class="col-md-12 col-sm-12 col-xs-12 infos">
              <div class="col-md-12 col-sm-12 col-xs-12">

              <div class="col-md-4 col-sm-4 col-xs-12 info-bloco">
                <div class="col-md-3 col-sm-4 col-xs-4 icon">
                  <div class="table">
                    <div class="table-cell">
                      <i class="glyphicon glyphicon-star"></i>
                    </div>
                  </div>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-8 content">
                  <div class="table">
                    <div class="table-cell">
                      <span class="blue-color">90% de desconto</span><br>na matr&iacute;cula<sup>1</sup>
                    </div>
                  </div>
                </div>
              </div><div class="col-md-4 col-sm-4 col-xs-12 info-bloco">
                <div class="col-md-3 col-sm-4 col-xs-4 icon">
                  <div class="table">
                    <div class="table-cell">
                      <i class="glyphicon glyphicon-education"></i>
                    </div>
                  </div>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-8 content">
                  <div class="table">
                    <div class="table-cell">
                      <span class="blue-color">At&#233; 35% de desconto*</span><br>nas mensalidades<sup>2</sup>
                    </div>
                  </div>
                </div>
              </div><div class="col-md-4 col-sm-4 col-xs-12 info-bloco">
                <div class="col-md-3 col-sm-4 col-xs-4 icon">
                  <div class="table">
                    <div class="table-cell">
                      <i class="glyphicon glyphicon-usd"></i>
                    </div>
                  </div>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-8 content">
                  <div class="table">
                    <div class="table-cell">
                      <span class="blue-color">Isento da taxa</span><br>de inscri&ccedil;&atilde;o
                    </div>
                  </div>
                </div>
              </div>
              <!-- <div class="col-md-4 col-sm-4 col-xs-12 info-bloco">
                <div class="col-md-3 col-sm-4 col-xs-4 icon">
                  <div class="table">
                    <div class="table-cell">
                      <i class="glyphicon glyphicon-ok"></i>
                    </div>
                  </div>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-8 content">
                  <div class="table">
                    <div class="table-cell">
                      <span class="blue-color">Curso de Extens&#227;o</span><br>gr&#225;tis<sup>3</sup>
                    </div>
                  </div>
                </div>
              </div> -->

            </div>
            </div>
            <!-- /END Infos -->
          </div>
          <!-- /END Oferta -->

          <!-- Open Formul&#225;rio -->
          <div class="col-md-12 col-sm-12 col-xs-12 formulario" ng-controller="FormController">

       <!-- <form name="myForm" id="webservice" action="http://177.22.37.162:81/webservices/cadastro_izy.asmx/Cadastro" method="POST" target="action"> -->
            <form name="myForm" id="webservice" action="" method="POST" target="action">
                <input type="text" size="50" name="Acesso" style="display: none;" value="6CgvsiW0D2HSsSa1foEdaQ==">
                <input type="text" size="50" name="IDExterno" style="display: none;" value="">
                <input type="text" size="50" name="RG" value="" style="display: none;">
                <input type="text" size="50" name="DataNasc" value="" style="display: none;">
                <input type="text" size="50" name="Cep" value="" style="display: none;">
                <input type="text" size="50" name="Endereco" value="" style="display: none;">
                <input type="text" size="50" name="Numero" value="" style="display: none;">
                <input type="text" size="50" name="Complemento" value="" style="display: none;">
                <input type="text" size="50" name="Bairro" value="" style="display: none;">
                <input type="text" size="50" name="Cidade" value="" style="display: none;">
                <input type="text" size="50" name="Estado" value="" style="display: none;">
                <input type="text" size="50" name="Deficiencia" value="" style="display: none;">
                <input type="text" size="50" name="DescricaoDeficiencia" value="" style="display: none;">
                <input type="text" size="50" name="DataProva" value="" style="display: none;" >
                <input type="text" size="50" name="CheckInMailer" value="" style="display: none;">
                <input type="text" size="50" name="Periodo" value="" style="display: none;">
                <input type="text" size="50" name="Valor" value="" style="display: none;">

                <!-- Curso -->
                <input type="text" size="50" name="Curso" value="Bacharelado em Arquitetura e Urbanismo" style="display: none;">

                <?php
                  // Pegar Query Strings
                  $string = basename($_SERVER['QUERY_STRING']);
                  $dados = explode('&',$string);
                  $total = count($dados);
                  $array = '';

                  $campanha = ltrim(strstr($dados[0], '='), '=');
                  $midia = ltrim(strstr($dados[1], '='), '=');
                  $consultor = ltrim(strstr($dados[2], '='), '=');

                  //Campanha - Hidden
                  echo '<input type="text" name="Campanha" value="'.$campanha.'" style="display:none;">';
                  echo '<input type="text" name="Midia" value="'.$midia.'" style="display:none;">';
                ?>

              <!-- Radios Processos -->
              <div class="col-md-12 list-radios">
                <div class="col-md-12"><h4>Escolha como deseja ingressar no Curso de Gradua&#231;&#227;o em Arquitetura e Urbanismo da Faculdade Ateneu:</h4></div>
                <div class="col-md-3" ng-repeat="ingresso in ingressos">
                  <input class="radio" id="{{ingresso.id}}" type="radio" title="{{ingresso.name}}" name="TipoInscricao" value="{{ingresso.value}}"  ng-model="$parent.s" ng-change="$parent.changehappened(ingresso)">
                  <label for="{{ingresso.id}}">{{ingresso.name}}</label>
                </div>
              </div>
              <!-- /END Radios Processos -->

              <!-- Dados Pessoais -->
              <div class="col-md-12">
                <div class="form-group col-md-3">
                  <input class="form-control" type="text" name="Nome" id="nome" ng-model="nome" placeholder="Nome" >
                </div>

                <div class="form-group col-md-3">
                  <input class="form-control" type="text" name="Email" id="email" ng-model="email" placeholder="E-mail" >
                </div>

                <div class="form-group col-md-3">
                  <input class="form-control" type="text" name="Telefone" id="telefone" ng-model="telefone" angular-mask="(00)00000-0000" placeholder="Telefone">
                </div>
                <!-- Unidade -->
                <div class="form-group col-md-3">
                  <select class="form-control" name="Unidade" id="unidade" ng-model="unidade">
                    <option value="">Selecione uma Unidade</option>
                    <option value="2" name="Messejana" title="Messejana">Messejana</option>
                    <!-- <option ng-repeat="unidade in unidades" value="{{unidade.value}}">{{unidade.name}}</option> -->
                  </select>
                </div>
              </div>

              <!-- /END Dados Pessoais -->

              <div class="col-md-12">
                <!-- CPF -->
                <!-- <div class="form-group col-md-6">
                  <input class="form-control" type="text" name="cpf" id="cpf" ng-cpf ng-model="cpf" angular-mask="000.000.000-00" placeholder="CPF">
                  <label class="error" ng-show="myForm.cpf.$invalid">Por favor, informe um CPF Válido!</label>
                  <label ng-hide="myForm.cpf.$valid"></label>
                </div> -->
              </div>

              <div id="mensagens"></div>

              <div class="col-md-12">
                <div class="form-group col-md-12">
                  <input type="submit" id="enviar" value="Inscreva-se Agora!" class="btn btn-default" onclick="mandaForm();">
                  <small>*Preencha todos os campos para ativar o botão de inscrição.</small>
                </div>
              </div>
            </form>

            <form class="hidden" id="e-goi" method="post" enctype="multipart/form-data" action="http://mkt.queroserateneu.com.br/w/1e10eDxue7bfwcfP5He578d6186" target="action2">
              <input type="hidden" name="lista" value="1">
              <input type="hidden" name="cliente" value="138749">
              <input type="hidden" name="lang" id="lang_id" value="br">
              <input type="hidden" name="formid" id="formid" value="60">
              <input name="fname_586" id="fname_586" value="{{nome}}" type="text">
              <input name="email_587" id="email_587" value="{{email}}" type="email" easyvalidation="false">
              <input name="campoe_15_591" id="campoe_15_591" value="{{telefone}}" type="text">
              <input type="text" name="campoe_34_605" id="campoe_34_605" value="{{cpf}}">
              <input type="text" name="campoe_19_588" id="{{selectedItem.idEgoi}}" value="{{selectedItem.value}}">
              <input type="text" name="campoe_20_589" id="campoe_20" value="{{unidade}}">

              <!-- Curso -->
              <input type="text" name="campoe_18_590" id="campoe_18" value="2" title="Bacharelado em Arquitetura e Urbanismo">

              <?php
                // Pegar URL
                $urlfull = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                $url = explode('/', $urlfull);
                $urlfinal = "$url[0]/$url[1]/$url[2]";

                // Pegar Query Strings
                $string = basename($_SERVER['QUERY_STRING']);
                $dados = explode('&',$string);
                $total = count($dados);
                $array = '';

                $campanha = ltrim(strstr($dados[0], '='), '=');
                $midia = ltrim(strstr($dados[1], '='), '=');
                $consultor = ltrim(strstr($dados[2], '='), '=');

                //Campanha - Hidden
                echo '<input type="text" name="campoe_16_592" id="campoe_16_592" value="'.$campanha.'">';
                echo '<input type="text" name="campoe_43_593" id="campoe_43_593" value="'.$midia.'">';
                echo '<input type="text" name="campoe_30_594" id="campoe_30_594" value="'.$consultor.'">';
                echo '<input type="text" name="campoe_17_601" id="campoe_17_601" value="'.$urlfinal.'">';
              ?>
            </form>

            <iframe class="hidden" src="action.php" name="action" frameborder="0"></iframe>
            <iframe class="hidden" src="action2.php" name="action2" frameborder="0"></iframe>

          </div>
          <!-- /END Formul&#225;rio -->

          <!-- Open Oferta -->
          <div class="col-md-12 col-sm-12 col-xs-12 desconto">
            <h3>Com o <strong>Curso de Gradua&#231;&#227;o em Arquitetura e Urbanismo da Faculdade Ateneu</strong>, voc&#234; tem al&#233;m de um curr&#237;culo moderno e professores de qualidade, diversos cursos de extens&#227;o. &#201; a sua oportunidade de se especializar em diversos campos e ter um mercado de trabalho muito mais favor&#225;vel quando sair com diploma na m&#227;o. Entendeu?</h3>
          </div>

          <!-- Infos -->
          <div class="col-md-12 col-sm-12 col-xs-12 infos">
            <div class="col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1 col-xs-10 col-xs-offset-1 info-bloco">
              <div class="col-md-3 col-sm-3 col-xs-3 icon">
                <div class="table">
                  <div class="table-cell">
                    <i class="glyphicon glyphicon-file"></i>
                  </div>
                </div>
              </div>
              <div class="col-md-9 col-sm-9 col-xs-9 content">
                <div class="table">
                  <div class="table-cell">
                    Sobre o Curso | <a href="http://fate.edu.br/curso/arquitetura-e-urbanismo/" class="bt" target="_blank" ><i class="glyphicon glyphicon-chevron-right"></i> Ver Mais</a> <br>
                    Edital 2016.2 | <a href="http://fate.edu.br/documentos/editais/vestibular/faculdade-ateneu-edital-vestibular-tradicional-20162.pdf" class="bt" target="_blank" ><i class="glyphicon glyphicon-chevron-right"></i> Ver Edital</a> <br>
                    Manual do Aluno | <a href="http://queroserateneu.com.br/assets/inc/doc/manual-do-aluno-fate-2016.pdf" class="bt" target="_blank" ><i class="glyphicon glyphicon-chevron-right"></i> Ver Manual</a>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1 col-xs-10 col-xs-offset-1 info-bloco">
              <div class="col-md-3 col-sm-3 col-xs-3 icon">
                <div class="table">
                  <div class="table-cell">
                    <i class="glyphicon glyphicon-map-marker"></i>
                  </div>
                </div>
              </div>
              <div class="col-md-9 col-sm-9 col-xs-9 content">
                <div class="table">
                  <div class="table-cell">
                    <h3 class="yellow-color">Unidade Ateneu</h3>
                    Messejana: <a href="#" class="bt" data-toggle="modal" data-target="#myModalMessejana"><i class="glyphicon glyphicon-chevron-right"></i> Ver Mapa</a><br>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- /END Infos -->

          <!-- Footer -->
          <footer>
            <div class="col-md-12 col-sm-12 col-xs-12 copy">
              <p>A <a href="http://fate.edu.br/curso/arquitetura-e-urbanismo/" target="_blank"><strong>GRADUA&Ccedil;&Atilde;O EM ARQUITETURA E URBANISMO</strong></a> DA FACULDADE ATENEU &#201; O CURSO QUE IR&#193; SUPRIR O MERCADO COM PROFISSIONAIS QUALIFICADOS PARA GERAR E SUPERVISIONAR PROJETOS DE EDIFICA&#199;&#195;O E CONSTRU&#199;&#195;O, APORTANDO COM NOVOS ESTILOS ARQUITET&#212;NICOS E CONTRIBUINDO COM O DESENVOLVIMENTO HABITACIONAL DO BRASIL.</p>
              <br>

              <promocoes curso="Arquitetura e Urbanismo" cursolink="arquitetura-e-urbanismo" data="23 de Setembro de 2016"></promocoes>

            </div>
          </footer>

        </div><!-- /END Table-Cell -->
      </div><!-- /END Table -->
    </div><!-- /END Container -->

    <!-- Modal Messejana -->
    <div class="modal fade" id="myModalMessejana" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <h4>Unidade Messejana:</h4>
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d127388.64581950518!2d-38.502645!3d-3.832665!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcb9df86346d13bc0!2sFaculdade+Ateneu!5e0!3m2!1spt-BR!2sus!4v1450900997679" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
        </div><!-- /END.modal-content -->
      </div><!-- /END.modal-dialog -->
    </div><!-- /END Modal mapa -->

     <!-- Modal Mapa -->
    <div class="modal fade" id="myModalAntBezerra" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
          <h4>Unidade Ant&#244;nio Bezerra:</h4>
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7962.650930929789!2d-38.597751!3d-3.739082!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7c74bba62e2a681%3A0x6954fb423c31a7d3!2sTravessa+S%C3%A3o+Vicente+de+Paula%2C+300+-+Ant%C3%B4nio+Bezerra%2C+Fortaleza+-+CE%2C+Brasil!5e0!3m2!1spt-BR!2sus!4v1450901088410" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
        </div><!-- /END.modal-content -->
      </div><!-- /END.modal-dialog -->
    </div><!-- /END Modal mapa -->
  </body>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69142272-1', 'auto');
  ga('send', 'pageview');

</script>

</html>
