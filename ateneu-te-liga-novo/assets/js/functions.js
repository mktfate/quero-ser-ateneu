$(document).ready(function(){

    $('#webservice').validate({ //(85)00000-0000 = 14 || 13
        rules: {
            // Curso:          { required: true},
            Nome:           { required: true, minlength: 4 },                                 // Nome
            Email:          { required: true, email: true },                                  //email
            Telefone:       { required: true, minlength: 13, maxlength: 14},                                                //telefone
            cpf:            { required: true, minlength: 14, maxlength: 14},                  //CPF
            TipoInscricao:  { required: true}                                                 //Tipo de Iscrição
        },

        messages: {
            // Curso:          { required: 'Por favor, selecione um curso.'},
            Nome:           { required: 'Por favor, digite seu nome.', minlength: 'Seu nome deve conter mais de 4 letras.' },                     // Nome
            Email:          { required: 'Por favor, informe o seu email.', email: 'Por favor, informe um email v&aacute;lido.' },                 //email
            Telefone:       { required: 'Por favor, informe o seu telefone.', minlength: 'Por favor, informe o seu telefone com 10 digitos', maxlength: 'Por favor, informe o seu CPF de 11 digitos'},                                                                    //telefone
            cpf:            { required: 'Por favor, informe o seu CPF.', minlength: 'Por favor, informe o seu CPF de 11 digitos', maxlength: 'Por favor, informe o seu CPF de 11 digitos'},                                                                         //CPF
            TipoInscricao:  { required: 'Por favor, selecione o tipo de contato.'}                                                                //Tipo de Inscrição
        },

        // Monta a mensagem em uma caixa separada
        errorContainer:"#mensagens",
        errorLabelContainer:"#mensagens ul",
        wrapper:"span"
    });

    $('#curso').validate({
        rules: {
            Curso:          { required: true},
            Nome:           { required: true, minlength: 4 },                                 // Nome
            Email:          { required: true, email: true },                                  //email
            Telefone:       { required: true, minlength: 13, maxlength: 14},                  //telefone
            cpf:            { required: true, minlength: 14, maxlength: 14},                  //CPF
            TipoInscricao:  { required: true}                                                 //Tipo de Iscrição
        },

        messages: {
            Curso:          { required: 'Por favor, informe o curso desejado.'},
            Nome:           { required: 'Por favor, digite seu nome.', minlength: 'Seu nome deve conter mais de 4 letras.' },                                                                          // Nome
            Email:          { required: 'Por favor, informe o seu email.', email: 'Por favor, informe um email v&aacute;lido.' },                                                                      //email
            Telefone:       { required: 'Por favor, informe o seu telefone.', minlength: 'Por favor, informe o seu telefone com 10 digitos', maxlength: 'Por favor, informe o seu CPF de 11 digitos'}, //telefone
            cpf:            { required: 'Por favor, informe o seu CPF.', minlength: 'Por favor, informe o seu CPF de 11 digitos', maxlength: 'Por favor, informe o seu CPF de 11 digitos'},            //CPF
            TipoInscricao:  { required: 'Por favor, selecione o tipo de contato.'}                                                                                                                     //Tipo de Inscrição
        },

        // Monta a mensagem em uma caixa separada
        errorContainer:"#mensagens",
        errorLabelContainer:"#mensagens ul",
        wrapper:"span"
    });

 });
