<!DOCTYPE html>
  <html ng-app="myApp" class="ng-scope" lang="pt-BR" xmlns:fb="http://ogp.me/ns/fb#" prefix="og: http://ogp.me/ns#" class=" js flexbox webgl no-touch geolocation hashchange history websockets rgba hsla multiplebgs backgroundsize borderimage textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage applicationcache svg svgclippaths mediaqueries no-regions supports"><!--<![endif]-->
  <head>
    <title>Ateneu Te Liga</title>
    <meta charset="utf-8" />
    <meta name="author" content="Faculdade Ateneu">
    <meta name="robots" content="noindex">

    <!-- CSS -->
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/main.css" rel="stylesheet">

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="bower_components/angular/angular.min.js"></script>
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.validate.js"></script>
    <script src="assets/js/angular-simple-mask.js"></script>
    <script src="assets/js/functions.js"></script>

    <!-- ALERT -->
    <script src="bower_components/sweetalert2/dist/sweetalert2.min.js"></script>
    <link rel="stylesheet" href="bower_components/sweetalert2/dist/sweetalert2.min.css">

    <!-- CPF -->
    <script src="bower_components/cpf_cnpj/build/cpf.js"></script>
    <script src="bower_components/ng-cpf-cnpj/lib/ngCpfCnpj.js"></script>
    <script src="bower_components/cpf_cnpj/build/cnpj.js"></script>

  </head>
  <body>
    <!-- MAIN -->
    <div class="container">

      <!-- INICIO FORM E-GOI -->
      <form id="e-goi" action="http://mkt.queroserateneu.com.br/w/6e1aeDxue4OIyVmjH3ea37-bc58" enctype="multipart/form-data"  method="POST"  target="action2" class="hidden" >
          <input type="hidden" name="lista" value="6">
          <input type="hidden" name="cliente" value="138749">
          <input type="hidden" name="lang" id="lang_id" value="br">
          <input type="hidden" name="formid" id="formid" value="70">
          <input name="fname_693" id="fname_693" value="{{nome}}" type="text">
          <input name="email_694" id="email_694" value="{{email}}" easyvalidation="false" type="email">
          <input name="campoe_55_695" id="campoe_55_695" value="{{telefone}}" type="text">
          <input name="campoe_56_696" id="campoe_56_696" value="{{cpf}}" type="text">
          <input name="campoe_60_697" id="campoe_60_697" value="{{curso}}" type="text">
          <input name="campoe_61_698" id="campoe_61_698" value="" type="text">
      </form>
      <!-- FIM - FORM E-GOI -->

      <!-- INICIO - FORM WEBSERVICE -->
      <!-- <form name="myForm" id="curso" action="http://177.22.37.162:81/webservices/cadastro_izy.asmx/Cadastro" method="POST" target="action"> -->
      <form name="myForm" id="curso" method="POST" target="action">
        <input class="frmInput" type="text" size="50" name="Acesso" style="display: none;" value="6CgvsiW0D2HSsSa1foEdaQ==">
        <input class="frmInput" type="text" size="50" name="IDExterno" style="display: none;" value="">
        <input class="frmInput" type="text" size="50" name="RG" value="" style="display: none;">
        <!--
        <input class="frmInput" type="text" size="50" name="Curso" value="" style="display: none;">
        -->
        <input class="frmInput" type="text" size="50" name="DataNasc" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Cep" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Endereco" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Numero" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Complemento" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Bairro" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Estado" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Deficiencia" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="DescricaoDeficiencia" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="DataProva" value="" style="display: none;" >
        <input class="frmInput" type="text" size="50" name="Campanha" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Midia" value="Ateneu te liga" style="display: none;">
        <input class="frmInput" type="text" size="50" name="CheckInMailer" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Periodo" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Valor" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Unidade" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="TipoInscricao" value="pos_graduacao" style="display: none;">

        <!-- Cidade -->
        <input class="frmInput" type="text" size="50" name="Cidade" value="" style="display: none;">

        <div class="form-group">
        <label for="curso">Curso</label>
        <select name="Curso" class="form-control" id="curso" ng-model="curso">
          <option value="">Selecione um Curso</option>
          <option value="">---</option>
          <option value="Direito Tributário Trabalhista e Previdenciário">Direito Tributário Trabalhista e Previdenciário</option>
          <option value="Direito Militar">Direito Militar</option>
          <option value="Direito Penal">Direito Penal</option>
          <option value="Direito Previdenciário">Direito Previdenciário</option>
          <option value="Direito Processual">Direito Processual</option>
          <option value="Perícia Forense">Perícia Forense</option>
          <option value="">---</option>
          <option value="Cultura e História Afro-Brasileira">Cultura e História Afro-Brasileira</option>
          <option value="Ensino de Espanhol">Ensino de Espanhol</option>
          <option value="Ensino de Geografia">Ensino de Geografia</option>
          <option value="Gestão Escolar">Gestão Escolar</option>
          <option value="Informatica na Educação">Informatica na Educação</option>
          <option value="Língua Portuguesa e Literatura">Língua Portuguesa e Literatura</option>
          <option value="Psicopedagogia">Psicopedagogia</option>
          <option value="">---</option>
          <option value="Engenharia de Segurança do Trabalho">Engenharia de Segurança do Trabalho</option>
          <option value="">---</option>
          <option value="Gestão Ambiental">Gestão Ambiental</option>
          <option value="Gestão do Design de Moda">Gestão do Design de Moda</option>
          <option value="MBA Administração e Negócios">MBA Administração e Negócios</option>
          <option value="MBA Controladoria e Finanças">MBA Controladoria e Finanças</option>
          <option value="MBA Gestão de Recursos Humanos">MBA Gestão de Recursos Humanos</option>
          <option value="MBA Gestão Estratégica de Marketing">MBA Gestão Estratégica de Marketing</option>
          <option value="MBA Operações Logísticas">MBA Operações Logísticas</option>
          <option value="">---</option>
          <option value="Auditoria Dos Sistemas de Saúde">Auditoria Dos Sistemas de Saúde</option>
          <option value="Citologia Clínica">Citologia Clínica</option>
          <option value="Educação Física Para Grupos Especiais">Educação Física Para Grupos Especiais</option>
          <option value="Enfermagem do Trabalho">Enfermagem do Trabalho</option>
          <option value="Especialização em Assistência em Cuidados Paliativos">Especialização em Assistência em Cuidados Paliativos</option>
          <option value="Farmacologia">Farmacologia</option>
          <option value="Fisiologia e Prescrição do Exercício">Fisiologia e Prescrição do Exercício</option>
          <option value="Fisioterapia Cardiorespiratória">Fisioterapia Cardiorespiratória</option>
          <option value="Fisioterapia Dermato-Funcional">Fisioterapia Dermato-Funcional</option>
          <option value="Fisioterapia na Saúde do Trabalhador e Ergonomia">Fisioterapia na Saúde do Trabalhador e Ergonomia</option>
          <option value="Gerontologia">Gerontologia</option>
          <option value="Metodologia do Ensino da Dança">Metodologia do Ensino da Dança</option>
          <option value="Saúde da Família">Saúde da Família</option>
          <option value="Saude do Trabalhador e Ergonomia">Saude do Trabalhador e Ergonomia</option>
          <option value="Segurança Alimentar e Nutricional">Segurança Alimentar e Nutricional</option>
          <option value="Vigilância Sanitária">Vigilância Sanitária</option>
          <option value="">---</option>
          <option value="Governança de TI">Governança de TI</option>
          <option value="MBA Gerenciamento de Projetos">MBA Gerenciamento de Projetos</option>
          <option value="Segurança em Redes de Computadores">Segurança em Redes de Computadores</option>
        </select>
        </div>

        <div class="form-group">
        <label for="nome">Nome</label>
        <input class="form-control" type="text" name="Nome" id="nome" ng-model="nome" >
        </div>

        <div class="form-group">
        <label for="email">E-mail</label>
        <input class="form-control" type="text" name="Email" id="email" ng-model="email" >
        </div>

        <div class="form-group">
        <label for="telefone">Telefone</label>
        <input class="form-control" type="text" name="Telefone" id="telefone" ng-model="telefone" angular-mask="(00)00000-0000" maxlengh="21">
        </div>

        <div class="form-group">
        <label for="cpf">CPF</label>
        <input class="form-control" type="text" name="cpf" id="cpf" ng-cpf ng-model="cpf" angular-mask="000.000.000-00">
        <p style="color: red" ng-show="myForm.cpf.$invalid">Por favor, informe um CPF Válido!</p>
        <p ng-hide="myForm.cpf.$valid"></p>
        </div>

        <input ng-disabled="!nome || !email || !telefone || !cpf ||!curso" type="submit" id="enviar" value="ENVIAR DADOS" class="btn btn-action" onclick="mandaForm();">

        <div id="mensagens"></div>
      </form>
      <!-- FIM FORM WEBSERVICE -->

      <iframe class="hidden" src="action.php" name="action" frameborder="0"></iframe>
      <iframe class="hidden" src="action2.php" name="action2" frameborder="0"></iframe>
    </div>

    <!-- INVIAR INFORMAÇÃO -->
    <script type="text/javascript">
      function mandaForm(){
        var enviar = $('#e-goi').submit();
        if(enviar == false){
          alert("Não Deu rock");
        }
        else {
          swal(
            'Dados Enviado Com Sucesso!',
            'Em breve entraremos em contato!',
            'success'
          );
        }
      }
    </script>

    <!-- CHAMAR ANGULAR DO CPF -->
    <script type="text/javascript">
      angular.module('myApp', ['ngCpfCnpj', 'angularMask']);
    </script>

  </body>
</html>
