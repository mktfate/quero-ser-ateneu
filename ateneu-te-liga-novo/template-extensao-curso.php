<?php
// Exit if accessed directly
if ( !defined('ABSPATH')) exit;

/**
 * Template Name: Template Extensão - Cursos
 * @package WordPress
 * @subpackage Grupo Ateneu, for WordPress
 * @since Grupo Ateneu, for WordPress 1.0
 */
?>
<?php get_header(); ?>

	<?php
		if ( is_front_page() && twentyfourteen_has_featured_posts() ) {
			// Include the featured content template.
			get_template_part( 'featured-content' );
		}
	?>
	<?php
		$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
		if (!$url == ""){
	?>
		<div class="banners" style="background-image: url(<?php echo $url; ?>)"></div>
	<?php } else { ?>
		<div class="banners banner-institucional"></div>
	<?php } ?>
	<?php the_breadcrumb(); ?>
	<section>
		<div class="container content" role="main">
			<!-- <div class="col-md-3">
				<?php get_sidebar(); ?>
			</div> -->
			<div class="col-md-12">
				<?php while ( have_posts() ) : the_post(); ?>
					<article>
						<header class="entry-header">
							<h1 class="entry-title" itemprop="headline"><?php the_title(); ?></h1>
						</header>
						<div class="alert alert-success" style="background-color: gold; color: #0c4b99; TEXT-TRANSFORM: uppercase; text-align: center; font-size: 18px; font-weight: bold">
						  Desconto Especial Para Alunos Técnico Ateneu.
						</div>
						<?php the_content(); ?>
					</article>
				<?php endwhile; ?>

				<div class="col-md-12" style="padding: 0">
					<a style="text-align: center;" class="btn btn-tra btn-lg" href="http://queroserateneu.com.br/cursos-tecnicos/?utm-campain=QsaTec20162&utm-souce=SitePolitecnica">Inscreva-se</a>
				</div>

		</div>
			</div>
		</div>
	</section>

	<!-- ATENEU TE LIGA -->
	<div class="destaque" style="background-color: white;">
		<div class="call-to-action">
			<div class="container">
				<div class="col-xs-5 col-xs-offset-1">
					<div class="ligue-para-ateneu">
						Quer saber mais? <strong>Ligue:</strong><br/>
						<span>(85) 3474-5151</span>
					</div>
				</div>
				<div class="col-xs-1 or">ou</div>
				<div class="col-xs-5">
					<a class="btn btn-action btn-lg ligueAgora" href="#ligue">Nós ligamos para você!</a>
				</div>
			</div>
		</div>
	</div>
	<div id="ligue" style="display: none">
		<!-- <?php echo do_shortcode('[contact-form-7 id="243" title="Ateneu te Liga"]') ?> -->
		<div class="col-md-12">
				<h3>ATENEU TE LIGA</h3>
				<p>Estamos aqui para facilitar o seu dia-a-dia e levamos isso muito a sério. Pensando nisso, nossa equipe de atendimento está a sua disposição,
				de segunda a sexta, das 8h às 18h. Apenas para Fortaleza e região metropolitana. <br><br>
				Precisamos dos seus dados para que o atendimento ocorra de forma satisfatória.
				</p>
				 <iframe src="http://queroserateneu.com.br/ateneu-te-liga-novo/politecnica-extensao.php" width="100%" height="500" frameborder="0" allowtransparency="true"></iframe>
			</div>
	</div>
<?php get_footer(); ?>
