<!DOCTYPE html>
  <html ng-app="myApp" class="ng-scope" lang="pt-BR" xmlns:fb="http://ogp.me/ns/fb#" prefix="og: http://ogp.me/ns#" class=" js flexbox webgl no-touch geolocation hashchange history websockets rgba hsla multiplebgs backgroundsize borderimage textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage applicationcache svg svgclippaths mediaqueries no-regions supports"><!--<![endif]-->
  <head>
    <title>Ateneu Te Liga</title>
    <meta charset="utf-8" />
    <meta name="author" content="Faculdade Ateneu">
    <meta name="robots" content="noindex">

    <!-- CSS -->
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/main.css" rel="stylesheet">

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="bower_components/angular/angular.min.js"></script>
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.validate.js"></script>
    <script src="assets/js/angular-simple-mask.js"></script>
    <script src="assets/js/functions.js"></script>

    <!-- ALERT -->
    <script src="bower_components/sweetalert2/dist/sweetalert2.min.js"></script>
    <link rel="stylesheet" href="bower_components/sweetalert2/dist/sweetalert2.min.css">

    <!-- CPF -->
    <script src="bower_components/cpf_cnpj/build/cpf.js"></script>
    <script src="bower_components/ng-cpf-cnpj/lib/ngCpfCnpj.js"></script>
    <script src="bower_components/cpf_cnpj/build/cnpj.js"></script>

  </head>
  <body>
    <!-- MAIN -->
    <div class="container">

      <!-- INICIO FORM E-GOI -->
      <form id="e-goi" action="http://mkt.queroserateneu.com.br/w/6e1ceDxue5pqUKdxqOe294739db" enctype="multipart/form-data" class="hidden" method="POST" target="action2" >
          <input type="hidden" name="lista" value="6">
          <input type="hidden" name="cliente" value="138749">
          <input type="hidden" name="lang" id="lang_id" value="br">
          <input type="hidden" name="formid" id="formid" value="72">
          <input name="fname_701" id="fname_701" value="{{nome}}" type="text">
          <input name="email_702" id="email_702" value="{{email}}" easyvalidation="false" type="email">
          <input name="campoe_55_703" id="campoe_55_703" value="{{telefone}}" type="text">
          <input name="campoe_56_704" id="campoe_56_704" value="{{cpf}}" type="text">
          <input name="campoe_60_705" id="campoe_60_705" value="{{curso}}" type="text">
          <input name="campoe_59_707" id="campoe_59_707" value="extensao" type="text">
      </form>
      <!-- FIM - FORM E-GOI -->

      <!-- INICIO - FORM WEBSERVICE -->
      <form name="myForm" id="curso" action="http://177.22.37.162:81/webservices/cadastro_izy.asmx/Cadastro" method="POST" target="action">
        <input class="frmInput" type="text" size="50" name="Acesso" style="display: none;" value="6CgvsiW0D2HSsSa1foEdaQ==">
        <input class="frmInput" type="text" size="50" name="IDExterno" style="display: none;" value="">
        <input class="frmInput" type="text" size="50" name="RG" value="" style="display: none;">
        <!--
          <input class="frmInput" type="text" size="50" name="Curso" value="" style="display: none;">
        -->
        <input class="frmInput" type="text" size="50" name="DataNasc" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Cep" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Endereco" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Numero" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Complemento" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Bairro" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Cidade" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Estado" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Deficiencia" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="DescricaoDeficiencia" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="DataProva" value="" style="display: none;" >
        <input class="frmInput" type="text" size="50" name="Campanha" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Midia" value="Ateneu te liga" style="display: none;">
        <input class="frmInput" type="text" size="50" name="CheckInMailer" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Periodo" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Valor" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Unidade" value="" style="display: none;">

        <input class="frmInput" type="text" size="50" name="TipoInscricao" value="extensao_tecnicos" style="display: none;">

        <div class="form-group">
        <label for="curso">Curso</label>
        <select name="Curso" class="form-control" id="curso" ng-model="curso">
          <optgroup label="Segurança do Trabalho">
            <option value="Percepções de Riscos Laborais">Percepções de Riscos Laborais</option>
            <option value="Comportamento Seguro">Comportamento Seguro</option>
            <option value="Gerenciamento de EPI's">Gerenciamento de EPI's</option>
          </optgroup>
          <optgroup label="Logística">
            <option value="Segurança na Movimentação e Transporte de Materiais">Segurança na Movimentação e Transporte de Materiais</option>
            <option value="Gestão do Transporte de Cargas Rodoviaria">Gestão do Transporte de Cargas Rodoviaria</option>
          </optgroup>
          <optgroup label="Saúde">
            <option value="Teoria, Prática e Diluição de Medicamentos">Teoria, Prática e Diluição de Medicamentos</option>
            <option value="Tecnicas de Curativos">Tecnicas de Curativos</option>
            <option value="Técnico de Enfermagem na Saúde do Trabalhador">Técnico de Enfermagem na Saúde do Trabalhador</option>
            <option value="Aprofundamento em Banco de Sangue e Hemotransfusão">Aprofundamento em Banco de Sangue e Hemotransfusão</option>
            <option value="Home Care">Home Care</option>
            <option value="Pratica em UTI">Pratica em UTI</option>
            <option value="Urgência e Emergência">Urgência e Emergência</option>
          </optgroup>
          <optgroup label="Edificações">
            <option value="AutoCad">AutoCad</option>
            <option value="Construção Sustentável">Construção Sustentável</option>
            <option value="Dosagem de Concreto">Dosagem de Concreto</option>
          </optgroup>
          <optgroup label="Estética">
            <option value="Formação em Dermo Estética - Novas Técnica Faciais">Formação em Dermo Estética - Novas Técnica Faciais</option>
            <option value="Peelings nas Disfunção Estética (Cristal, Diamante)">Peelings nas Disfunção Estética (Cristal, Diamante)</option>
            <option value="Microagulhamento - Técnicas Combinadas">Microagulhamento - Técnicas Combinadas</option>
          </optgroup>
          <optgroup label="Técnologia">
            <option value="C#.NET">C#.NET</option>
            <option value="PHP">PHP</option>
            <option value="JAVA">JAVA</option>
            <option value="SQL">SQL</option>
          </optgroup>
        </select>
        </div>

        <div class="form-group">
        <label for="nome">Nome</label>
        <input class="form-control" type="text" name="Nome" id="nome" ng-model="nome" >
        </div>

        <div class="form-group">
        <label for="email">E-mail</label>
        <input class="form-control" type="text" name="Email" id="email" ng-model="email" >
        </div>

        <div class="form-group">
        <label for="telefone">Telefone</label>
        <input class="form-control" type="text" name="Telefone" id="telefone" ng-model="telefone" angular-mask="(00)00000-0000" maxlengh="21">
        </div>

        <div class="form-group">
        <label for="cpf">CPF</label>
        <input class="form-control" type="text" name="cpf" id="cpf" ng-cpf ng-model="cpf" angular-mask="000.000.000-00">
        <p style="color: red" ng-show="myForm.cpf.$invalid">Por favor, informe um CPF Válido!</p>
        <p ng-hide="myForm.cpf.$valid"></p>
        </div>

        <input ng-disabled="!nome || !email || !telefone || !cpf || !curso" type="submit" id="enviar" value="ENVIAR DADOS" class="btn btn-action" onclick="mandaForm();">

        <div id="mensagens"></div>
      </form>
      <!-- FIM FORM WEBSERVICE -->

      <iframe class="hidden" src="action.php" name="action" frameborder="0"></iframe>
      <iframe class="hidden" src="action2.php" name="action2" frameborder="0"></iframe>
    </div>

    <!-- INVIAR INFORMAÇÃO -->
    <script type="text/javascript">
      function mandaForm(){
        var enviar = $('#e-goi').submit();
        if(enviar == false){
          alert("Não Deu rock");
        }
        else {
          swal(
            'Dados Enviado Com Sucesso!',
            'Em breve entraremos em contato!',
            'success'
          );
        }
      }
    </script>

    <!-- CHAMAR ANGULAR DO CPF -->
    <script type="text/javascript">
      angular.module('myApp', ['ngCpfCnpj', 'angularMask']);
    </script>

  </body>
</html>
