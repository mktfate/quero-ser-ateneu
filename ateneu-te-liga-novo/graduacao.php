<!DOCTYPE html>
  <html ng-app="myApp" class="ng-scope" lang="pt-BR" xmlns:fb="http://ogp.me/ns/fb#" prefix="og: http://ogp.me/ns#" class=" js flexbox webgl no-touch geolocation hashchange history websockets rgba hsla multiplebgs backgroundsize borderimage textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage applicationcache svg svgclippaths mediaqueries no-regions supports"><!--<![endif]-->
  <head>
    <title>Ateneu Te Liga</title>
    <meta charset="utf-8" />
    <meta name="author" content="Faculdade Ateneu">
    <meta name="robots" content="noindex">

    <!-- CSS -->
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/main.css" rel="stylesheet">

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="bower_components/angular/angular.min.js"></script>
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.validate.js"></script>
    <script src="assets/js/angular-simple-mask.js"></script>
    <script src="assets/js/functions.js"></script>

    <!-- ALERT -->
    <script src="bower_components/sweetalert2/dist/sweetalert2.min.js"></script>
    <link rel="stylesheet" href="bower_components/sweetalert2/dist/sweetalert2.min.css">

    <!-- CPF -->
    <script src="bower_components/cpf_cnpj/build/cpf.js"></script>
    <script src="bower_components/ng-cpf-cnpj/lib/ngCpfCnpj.js"></script>
    <script src="bower_components/cpf_cnpj/build/cnpj.js"></script>

  </head>
  <body>
    <!-- MAIN -->
    <div class="container">

      <!-- INICIO FORM E-GOI -->
      <form id="e-goi" action="http://mkt.queroserateneu.com.br/w/6e19eDxue4OIyVhgWqe932d3486" enctype="multipart/form-data"  method="POST"  target="action2" class="hidden">
          <input type="hidden" name="lista" value="6">
          <input type="hidden" name="cliente" value="138749">
          <input type="hidden" name="lang" id="lang_id" value="br">
          <input type="hidden" name="formid" id="formid" value="69">
          <input name="fname_682" id="fname_682" value="{{nome}}" type="text">
          <input name="email_683" id="email_683" value="{{email}}" easyvalidation="false" type="email">
          <input name="campoe_55_686" id="campoe_55_686" value="{{telefone}}" type="text">
          <input name="campoe_56_687" id="campoe_56_687" value="{{cpf}}" type="text">
          <input name="campoe_59_691" id="campoe_59_691" value="{{tipoinscricao}}" type="text">
          <input name="campoe_61_692" id="campoe_61_692" value="fortaleza" type="text">
      </form>
      <!-- FIM - FORM E-GOI -->

      <!-- INICIO - FORM WEBSERVICE -->
      <!-- <form name="myForm" id="webservice" action="http://177.22.37.162:81/webservices/cadastro_izy.asmx/Cadastro" method="POST" target="action"> -->
      <form name="myForm" id="webservice" method="POST" target="action">
        <input class="frmInput" type="text" size="50" name="Acesso" style="display: none;" value="6CgvsiW0D2HSsSa1foEdaQ==">
        <input class="frmInput" type="text" size="50" name="IDExterno" style="display: none;" value="">
        <input class="frmInput" type="text" size="50" name="RG" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Curso" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="DataNasc" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Cep" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Endereco" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Numero" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Complemento" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Bairro" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Estado" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Deficiencia" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="DescricaoDeficiencia" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="DataProva" value="" style="display: none;" >
        <input class="frmInput" type="text" size="50" name="Campanha" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Midia" value="Ateneu te liga" style="display: none;">
        <input class="frmInput" type="text" size="50" name="CheckInMailer" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Periodo" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Valor" value="" style="display: none;">
        <input class="frmInput" type="text" size="50" name="Unidade" value="" style="display: none;">

        <input class="frmInput" type="text" size="50" name="Cidade" value="fortaleza" style="display: none;">

        <div class="form-group">
        <label for="nome">Nome</label>
        <input class="form-control" type="text" name="Nome" id="nome" ng-model="nome" >
        </div>

        <div class="form-group">
        <label for="email">E-mail</label>
        <input class="form-control" type="text" name="Email" id="email" ng-model="email" >
        </div>

        <div class="form-group">
        <label for="telefone">Telefone</label>
        <input class="form-control" type="text" name="Telefone" id="telefone" ng-model="telefone" angular-mask="(00)00000-0000" maxlengh="21">
        </div>

        <div class="form-group">
        <label for="cpf">CPF</label>
        <input class="form-control" type="text" name="cpf" id="cpf" ng-cpf ng-model="cpf" angular-mask="000.000.000-00">
        <p style="color: red" ng-show="myForm.cpf.$invalid">Por favor, informe um CPF Válido!</p>
        <p ng-hide="myForm.cpf.$valid"></p>
        </div>

        <div class="form-group">
        <label for="tipoinscricao">Tipo de Contato</label>
        <select name="TipoInscricao" class="form-control" id="tipoinscricao" ng-model="tipoinscricao">
          <option order="1" value="" checked>Selecione uma opção</option>
          <option order="2" value="vestibular">Vestibular</option>
          <option order="3" value="transferencia">Transferência de Cursos</option>
          <option order="4" value="segunda_graduacao">Segunda Graduação</option>
          <option order="5" value="enem">ENEM</option>
          <option order="6" value="prouni">PROUNI</option>
          <option order="7" value="matricula_novatos">Matrícula Novatos</option>
          <option order="8" value="tecnico">Cursos Técnicos</option>
          <option order="9" value="pos_graduacao">Pós-Graduação</option>
          <option order="10" value="extensao">Cursos de Extensão</option>
          <option order="11" value="endereco_unidades">Endereço de Unidades</option>
          <!--
          <option order="12" value="convenio_empresa">Convênio-Empresas</option>
          <option order="13" value="convenio_escola">Convênio-Escolas</option>
          -->
        </select>
        </div>
        <input ng-disabled="!nome || !email || !telefone || !cpf || !tipoinscricao" type="submit" id="enviar" value="ENVIAR DADOS" class="btn btn-action" onclick="mandaForm();">

        <div id="mensagens"></div>
      </form>
      <!-- FIM FORM WEBSERVICE -->

      <iframe class="hidden" src="action.php" name="action" frameborder="0"></iframe>
      <iframe class="hidden" src="action2.php" name="action2" frameborder="0"></iframe>
    </div>

    <!-- INVIAR INFORMAÇÃO -->
    <script type="text/javascript">
      function mandaForm(){
        var enviar = $('#e-goi').submit();
        if(enviar == false){
          alert("Não Deu rock");
        }
        else {
          swal(
            'Dados Enviado Com Sucesso!',
            'Em breve entraremos em contato!',
            'success'
          );
        }
      }
    </script>

    <!-- CHAMAR ANGULAR DO CPF -->
    <script type="text/javascript">
      angular.module('myApp', ['ngCpfCnpj', 'angularMask']);
    </script>

  </body>
</html>
