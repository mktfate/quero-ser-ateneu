﻿<!DOCTYPE html>
  <!--[if lt IE 7 ]><html
  lang="pt-BR" prefix="og: http://ogp.me/ns#" class="ie6"> <![endif]-->
  <!--[if IE 7 ]><html
  lang="pt-BR" prefix="og: http://ogp.me/ns#" class="ie7"> <![endif]-->
  <!--[if IE 8 ]><html
  lang="pt-BR" prefix="og: http://ogp.me/ns#" class="ie8"> <![endif]-->
  <!--[if IE 9 ]><html
  lang="pt-BR" prefix="og: http://ogp.me/ns#" class="ie9"> <![endif]-->
  <!--[if (gt IE 9)|!(IE)]><!-->
  <html lang="pt-BR" ng-app="myApp" ng-app xmlns:fb="http://ogp.me/ns/fb#" prefix="og: http://ogp.me/ns#" class="js flexbox webgl no-touch geolocation hashchange history websockets rgba hsla multiplebgs backgroundsize borderimage textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage applicationcache svg svgclippaths mediaqueries no-regions supports"><!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cursos T&#233;cnicos Ateneu</title>
    <meta name="description" content="Com os Cursos T&#233;cnicos da Ateneu voc&#234; aprende o que o mercado precisa antes de todo mundo. | Matricule-se Agora nos Cursos T&#233;cnicos Ateneu.">
    <meta name="keywords" content="cursos t&#233;cnicos em fortaleza, fate, faculdade ateneu, ateneu, cursos profissionalizantes, polit&#233;cnica, cursos livres, cursos, emprego, mercado de trabalho">
    <meta name="author" content="Escola Polit&#233;nica Ateneu">
    <meta name="robots" content="noodp,noydir">
    <link rel="shortcut icon" href="assets/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">
    <link href="http://queroserateneu.com.br/cursos-tecnicos/assets/images/post-tecnicos.jpg" rel="image_src"/>

    <meta property="og:locale" content="pt_BR">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Matricule-se agora e ganhe um Desconto Especial!">
    <meta property="og:description" content="Na Ateneu voc&#234; aprende o que o mercado precisa antes de todo mundo.">
    <meta property="og:url" content="http://queroserateneu.com.br/cursos-tecnicos/?<?php $string = basename($_SERVER['QUERY_STRING']); echo $string ?>">
    <meta property="og:site_name" content="Cursos Técnicos Ateneu">
    <meta property="og:image" content="http://queroserateneu.com.br/cursos-tecnicos/assets/images/post-tecnicos.jpg">
    <meta property="og:image:width" content="800">
    <meta property="og:image:height" content="419">

    <!-- CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/main.css" rel="stylesheet">


    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/angular.min.js"></script>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.validate.js"></script>
    <script src="assets/js/angular-simple-mask.js"></script>
    <script src="assets/js/functions.js"></script>
    <script src="assets/js/app.js"></script>

    <!-- CPF -->
    <script src="assets/js/cpf.min.js"></script>
    <script src="assets/js/ngCpfCnpj.js"></script>

  </head>
  <body>
    <!-- MAIN -->
    <div class="container">
          <!-- Destaque -->
          <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 destaque">
            <div class="col-md-5 col-sm-5 hidden-xs imagem">
              <picture>
                <img src="assets/images/modelos-cursos-tecnicos-ateneu.png" alt="Cursos T&#233;cnicos Ateneu">
              </picture>
            </div>

            <div class="col-md-7 col-sm-7 col-xs-12 texto content-left">
                <h3>#T&#212;NA<span>ATENEU</span></h3>
                <h1><span>CURSOS T&#201;CNICOS</span></h1>
                <h2>Com os Cursos T&#233;cnicos da Ateneu voc&#234; aprende o que o mercado precisa antes de todo mundo.</h2>
            </div>
          </div>
          <!-- Close Destaque -->

          <!-- Open Oferta -->
          <div class="col-md-12 col-sm-12 col-xs-12 desconto">
            <h3>MATRICULE-SE E GANHE 90% DE DESCONTO NA MATR&#205;CULA
              <br><small>+ 40% NAS 6 PRIMEIRAS MENSALIDADES + 10% NO RESTANTE DO CURSO*.</small></h3>
          </div>

          <!-- Open Formul&#225;rio -->
          <div class="col-md-12 col-sm-12 col-xs-12 formulario">

            <!-- <form name="myForm" id="webservice" action="" method="POST" target="action" novalidate> -->
              <form name="myForm" id="webservice" action="http://177.22.37.162:81/webservices/cadastro_izy.asmx/Cadastro" method="POST" target="action" novalidate>
                <input type="text" size="50" name="Acesso" style="display: none;" value="6CgvsiW0D2HSsSa1foEdaQ==">
                <input type="text" size="50" name="IDExterno" style="display: none;" value="">
                <input type="text" size="50" name="RG" value="" style="display: none;">
                <input type="text" size="50" name="DataNasc" value="" style="display: none;">
                <input type="text" size="50" name="Cep" value="" style="display: none;">
                <input type="text" size="50" name="Endereco" value="" style="display: none;">
                <input type="text" size="50" name="Numero" value="" style="display: none;">
                <input type="text" size="50" name="Complemento" value="" style="display: none;">
                <input type="text" size="50" name="Bairro" value="" style="display: none;">
                <input type="text" size="50" name="Cidade" value="" style="display: none;">
                <input type="text" size="50" name="Estado" value="" style="display: none;">
                <input type="text" size="50" name="Deficiencia" value="" style="display: none;">
                <input type="text" size="50" name="DescricaoDeficiencia" value="" style="display: none;">
                <input type="text" size="50" name="DataProva" value="" style="display: none;" >
                <input type="text" size="50" name="CheckInMailer" value="" style="display: none;">
                <input type="text" size="50" name="Periodo" value="" style="display: none;">
                <input type="text" size="50" name="Valor" value="" style="display: none;">
                <input type="text" size="50" name="TipoInscricao" value="tecnico" style="display: none;">

                <?php
                  // Pegar Query Strings
                  $string = basename($_SERVER['QUERY_STRING']);
                  $dados = explode('&',$string);
                  $total = count($dados);
                  $array = '';

                  $campanha = ltrim(strstr($dados[0], '='), '=');
                  $midia = ltrim(strstr($dados[1], '='), '=');
                  $consultor = ltrim(strstr($dados[2], '='), '=');

                  //Campanha - Hidden
                  echo '<input type="text" name="Campanha" value="'.$campanha.'" style="display:none;">';
                  echo '<input type="text" name="Midia" value="'.$midia.'" style="display:none;">';
                ?>

              <!-- Dados Pessoais -->
              <div class="col-md-12">
                <div class="form-group col-md-3">
                  <input class="form-control" type="text" name="Nome" id="nome" ng-model="nome" placeholder="Nome" >
                </div>

                <div class="form-group col-md-3">
                  <input class="form-control" type="text" name="Email" id="email" ng-model="email" placeholder="E-mail" >
                </div>

                <div class="form-group col-md-3">
                  <input class="form-control" type="text" name="Telefone" id="telefone" ng-model="telefone" angular-mask="(00)00000-0000" placeholder="Telefone">
                </div>

                <div class="form-group col-md-3">
                  <input class="form-control" type="text" name="cpf" id="cpf" ng-cpf ng-model="cpf" angular-mask="000.000.000-00" placeholder="CPF">
                  <label class="error" ng-show="myForm.cpf.$invalid">Por favor, informe um CPF Válido!</label>
                  <label ng-hide="myForm.cpf.$valid"></label>
                </div>
              </div>
              <!-- /END Dados Pessoais -->

              <div class="col-md-12">
                <!-- Unidade -->
                <div class="form-group col-md-6">
                  <select class="form-control" name="Unidade" id="unidade" ng-model="unidade">
                    <option selected="selected" value="" title="Unidade Ateneu"> Escolha uma Unidade</option>
                    <option value="Antonio Bezerra" title="Antonio Bezerra">Ant&#244;nio Bezerra</option>
                    <!-- <option value="Aldeota" title="Aldeota">Aldeota</option> -->
                    <option value="Messejana" title="Messejana">Messejana</option>
                    <option value="Pecem" title="Pecem">Pec&#233;m</option>
                  </select>
                </div>

                <!-- Curso -->
                <div class="form-group col-md-6">
                  <select name="Curso" class="form-control" id="curso" ng-model="curso">
                    <option value="">Selecione um Curso</option>
                    <option value="Técnico em Administração">Técnico em Administração</option>
                    <option value="Técnico em Edificações">Técnico em Edificações</option>
                    <option value="Técnico em Enfermagem">Técnico em Enfermagem</option>
                    <option value="Técnico em Estética">Técnico em Estética</option>
                    <option value="Técnico em Informática">Técnico em Informática</option>
                    <option value="Técnico em Logística">Técnico em Logística</option>
                    <option value="Técnico em Manutenção e Suporte em Informática">Técnico em Manutenção e Suporte em Informática</option>
                    <option value="Técnico em Segurança do Trabalho">Técnico em Segurança do Trabalho</option>
                  </select>
                </div>
              </div>

              <div id="mensagens"></div>

              <div class="col-md-12">
                <div class="form-group col-md-12">
             <!-- <input type="submit" id="enviar" value="Inscreva-se Agora!" class="btn btn-default" onclick="mandaForm();"> -->
                  <input type="submit" id="enviar" value="Inscreva-se Agora!" class="btn btn-default" onclick="mandaForm();">
                  <small>*Preencha todos os campos para ativar o botão de inscrição.</small>
                </div>
              </div>
            </form>

            <form class="hidden" id="e-goi" method="post" enctype="multipart/form-data" action="http://mkt.queroserateneu.com.br/w/4eSeDxuefkyg4791Beb556798d" target="action2">
              <input type="hidden" name="lista" value="4">
              <input type="hidden" name="cliente" value="138749">
              <input type="hidden" name="lang" id="lang_id" value="br">
              <input type="hidden" name="formid" id="formid" value="52">
              <input type="text" name="fname_455" id="fname_455" value="{{nome}}">
              <input type="text" name="email_456" id="email_456" value="{{email}}">
              <input type="text" name="campoe_11_458" id="campoe_11_458" value="{{telefone}}">
              <input type="text" name="campoe_40_468" id="campoe_40_468" value="{{cpf}}">
              <input type="text" name="campoe_38_466" id="campoe_38_466" value="{{unidade}}">
              <input type="text" name="campoe_37_465" id="campoe_37_465" value="{{curso}}">

               <?php
                  // Pegar URL
                  $urlfull = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                  $url = explode('/', $urlfull);
                  $urlfinal = "$url[0]/$url[1]";

                  // Pegar Query Strings
                  $string = basename($_SERVER['QUERY_STRING']);
                  $dados = explode('&',$string);
                  $total = count($dados);
                  $array = '';

                  $campanha = ltrim(strstr($dados[0], '='), '=');
                  $midia = ltrim(strstr($dados[1], '='), '=');
                  $consultor = ltrim(strstr($dados[2], '='), '=');

                  //Campanha - Hidden
                  echo '<input type="text" name="campoe_13_460" id="campoe_13_460" value="'.$campanha.'">';
                  echo '<input type="text" name="campoe_39_467" id="campoe_39_467" value="'.$midia.'">';
                  echo '<input type="text" name="campoe_36_462" id="campoe_36_462" value="'.$consultor.'">';
                  echo '<input type="text" name="campoe_14_461" id="campoe_14_461" value="'.$urlfinal.'">';
                ?>
                 <div id="mensagens"></div>
            </form>

            <iframe class="hidden" src="action.php" name="action" frameborder="0"></iframe>
            <iframe class="hidden" src="action2.php" name="action2" frameborder="0"></iframe>
          </div>
          <!-- /END Formul&#225;rio -->

          <!-- Footer -->
          <footer>
            <div class="col-md-12 col-sm-12 col-xs-12 copy">
              <h4>S&#243; na Ateneu voc&#234; encontra <strong>Cursos T&#233;cnicos</strong> desenvolvidos para atender as empresas da regi&#227;o e come&#231;ar sua carreira profissional à frente de todo mundo. E o melhor, voc&#234; pode fazer isso enquanto ainda cursa o ensino m&#233;dio. Comece a pensar no seu futuro hoje mesmo, fa&#231;a seu curso t&#233;cnico na Ateneu. Entendeu?</h4>
              <br>
              <p>*Promo&#231;&#227;o 90% de desconto na matr&#237;cula + 40% nas 6 primeiras mensalidades + 10% no restante dos Cursos T&#233;cnicos Ateneu v&#225;lida por tempo limitado.
                </br>Corra e garanta a sua vaga!
                  Promoção não cumulativa com outras promoções e bolsas da Faculdade Ateneu.<br class="hidden-xs hidden-sm">
              Promo&#231;&#227;o n&#227;o cumulativa com outras promo&#231;&#245;es e bolsas da Faculdade Ateneu.</p>
              <br>

              <h4><strong>Conhe&#231;a os Cursos T&#233;cnicos da Ateneu: <br class="visible-xs visible-sm">  <a href="#" class="bt" data-toggle="modal" data-target="#myModalAldeota"><i class="glyphicon glyphicon-map-marker"></i> Aldeota</a> | <a href="#" class="bt" data-toggle="modal" data-target="#myModalAntBezerra"><i class="glyphicon glyphicon-map-marker"></i> Ant. Bezerra</a> | <a href="#" class="bt" data-toggle="modal" data-target="#myModalMessejana"> <i class="glyphicon glyphicon-map-marker"></i> Messejana</a> | <a href="#" class="bt" data-toggle="modal" data-target="#myModalPecem"> <i class="glyphicon glyphicon-map-marker"></i> Pec&#233;m</a></strong></h4>
              <p><a href="http://cursostecnicosfortaleza.com.br/tecnico-em-administracao/" target="_blank">Curso T&#233;cnico em Administra&ccedil;&atilde;o</a>,
              <a href="http://cursostecnicosfortaleza.com.br/tecnico-em-edificacoes/" target="_blank">Curso T&#233;cnico em Edifica&#231;&#245;es</a>,
              <a href="http://cursostecnicosfortaleza.com.br/tecnico-em-enfermagem/" target="_blank">Curso T&#233;cnico em Enfermagem</a>,
              <a href="http://cursostecnicosfortaleza.com.br/tecnico-em-estetica/" target="_blank">Curso T&#233;cnico em Est&#233;tica</a>,
              <a href="http://cursostecnicosfortaleza.com.br/tecnico-em-informatica/" target="_blank">Curso T&#233;cnico em Inform&#225;tica</a>,
              <a href="http://cursostecnicosfortaleza.com.br/tecnico-em-logistica/" target="_blank">Curso T&#233;cnico em Loj&#237;stica</a>,
              <a href="http://cursostecnicosfortaleza.com.br/tecnico-em-manutencao-de-microcomputadores/" target="_blank">Curso T&#233;cnico em Manuten&#231;&#227;o de Microcomputadores</a>,
              <a href="http://cursostecnicosfortaleza.com.br/tecnico-em-seguranca-trabalho/" target="_blank">Curso T&#233;cnico em Seguran&#231;a do Trabalho</a>.
              </p>
              <br>
              <p>&#169; 2016 Faculdade Ateneu. Todos os direitos reservados. | Fone: 0800 006 1717</p>
            </div>
            <!-- Logo -->
            <div class="col-md-12 col-sm-12 col-xs-12 logo">
              <picture>
                <a href="http://cursostecnicosfortaleza.com.br/" target="_black">
                <img src="assets/images/logo-escola-politecnica-ateneu-blue.png" alt="Faculdade Ateneu | Construindo Valores">
                </a>
              </picture>
            </div>
          </footer>

    </div><!-- /END Container -->

    <!-- Modal Messejana -->
    <div class="modal fade" id="myModalMessejana" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <p>Unidade Messejana:</p>
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d127388.64581950518!2d-38.502645!3d-3.832665!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcb9df86346d13bc0!2sFaculdade+Ateneu!5e0!3m2!1spt-BR!2sus!4v1450900997679" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
        </div><!-- /END.modal-content -->
      </div><!-- /END.modal-dialog -->
    </div><!-- /END Modal mapa -->

    <!-- Modal Ant Bezerra -->
    <div class="modal fade" id="myModalAntBezerra" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
          <p>Unidade Ateneu Ant&#244;nio:</p>
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7962.650930929789!2d-38.597751!3d-3.739082!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7c74bba62e2a681%3A0x6954fb423c31a7d3!2sTravessa+S%C3%A3o+Vicente+de+Paula%2C+300+-+Ant%C3%B4nio+Bezerra%2C+Fortaleza+-+CE%2C+Brasil!5e0!3m2!1spt-BR!2sus!4v1450901088410" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
        </div><!-- /END.modal-content -->
      </div><!-- /END.modal-dialog -->
    </div><!-- /END Modal mapa -->

    <!-- Modal Ant Montese -->
    <div class="modal fade" id="myModalAldeota" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <p>Unidade Aldeota:</p>
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15925.270847912956!2d-38.512897!3d-3.740789!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7c748f68bf06ec7%3A0x56f510431f03f3a!2sR.+Carlos+Vasconcelos%2C+1774+-+Meireles%2C+Fortaleza+-+CE%2C+60115-170%2C+Brasil!5e0!3m2!1spt-BR!2sus!4v1463773478587" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
        </div><!-- /END.modal-content -->
      </div><!-- /END.modal-dialog -->
    </div><!-- /END Modal mapa -->
    <!-- Modal Pecém -->
    <div class="modal fade" id="myModalPecem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
          <p>Unidade Pec&#233;m:</p>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3982.146126301108!2d-38.83791938482413!3d-3.553779497413742!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7c0dabb28d80e4d%3A0x4db0ab850a86b063!2sEducandario+Francisca+Ferreira+Martins!5e0!3m2!1spt-BR!2sus!4v1452632014146" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
        </div><!-- /END.modal-content -->
      </div><!-- /END.modal-dialog -->
    </div><!-- /END Modal mapa -->

  </body>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69142272-1', 'auto');
  ga('send', 'pageview');

</script>

</html>
