﻿<!DOCTYPE html>
  <!--[if lt IE 7 ]><html
  lang="pt-BR" prefix="og: http://ogp.me/ns#" class="ie6"> <![endif]-->
  <!--[if IE 7 ]><html
  lang="pt-BR" prefix="og: http://ogp.me/ns#" class="ie7"> <![endif]-->
  <!--[if IE 8 ]><html
  lang="pt-BR" prefix="og: http://ogp.me/ns#" class="ie8"> <![endif]-->
  <!--[if IE 9 ]><html
  lang="pt-BR" prefix="og: http://ogp.me/ns#" class="ie9"> <![endif]-->
  <!--[if (gt IE 9)|!(IE)]><!-->
  <html lang="pt-BR" xmlns:fb="http://ogp.me/ns/fb#" prefix="og: http://ogp.me/ns#" class=" js flexbox webgl no-touch geolocation hashchange history websockets rgba hsla multiplebgs backgroundsize borderimage textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage applicationcache svg svgclippaths mediaqueries no-regions supports"><!--<![endif]-->
  <head>
    <title>Seja Bem-vindo à Escola Polit&#233;nica Ateneu!</title>
    <meta name="description" content="Com os Cursos T&#233;cnicos da Ateneu voc&#234; aprende o que o mercado precisa antes de todo mundo. | Matricule-se Agora nos Cursos T&#233;cnicos Ateneu.">
    <meta name="author" content="Escola Polit&#233;nica Ateneu">
    <meta name="robots" content="noindex" />
    <link rel="shortcut icon" href="assets/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">

    <!-- CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/main.css" rel="stylesheet">

<!-- Facebook Pixel Code - ANTIGO -->
<!-- <script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '765924773552744');
fbq('track', 'Lead');

</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=765924773552744&ev=Lead&noscript=1"
/></noscript> -->
<!-- End Facebook Pixel Code -->

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '1015554161890641');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1015554161890641&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

  </head>

  <body>
    <!-- MAIN -->
    <div class="container">
      <div class="table">
        <div class="table-cell">

          <!-- Destaque -->
          <div class="col-md-12 col-sm-12 col-xs-12 destaque confirma">
            <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12 col-xs-offset-0 texto">
                <h2 class="yellow-color">Parab&#233;ns pela sua inscri&#231;&#227;o!</h2>
                <h1>
                  SEJA BEM-VINDO &#192; <br> ESCOLA POLITÉNICA ATENEU
                </h1>
                <p>Acesse o seu e-mail e siga as nossas instru&#231;&#245;es para obter a oferta especial que preparamos para voc&#234;. Obrigado por fazer suas
                  pr&#233;-inscri&#231;&#227;o em nosso vestibular. Acesse o seu e-mail agora!<br><br>
                  <a href="https://mail.google.com/" target="_blank"><img src="assets/images/gmail-icon.png" alt="Acessar Gmail"></a>
                  <a href="https://login.live.com/" target="_blank"><img src="assets/images/outlook-icon.png" alt="Acessar Outlook"></a>
                  <a href="http://br.mail.yahoo.com/" target="_blank"><img src="assets/images/yahoo-icon.png" alt="Acessar Yahoo Mail"></a>
                </p>
            </div>
          </div>
          <!-- Close Destaque -->

        </div><!-- /END Table-Cell -->
      </div><!-- /END Table -->
    </div><!-- /END Container -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.validate.js"></script>
    <script type="text/javascript" src="assets/js/jquery.mask.js"></script>
    <script type="text/javascript" src="assets/js/functions.js"></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69142272-1', 'auto');
  ga('send', 'pageview');

</script>

  </body>
</html>
