var app = angular.module('myApp', ['ngCpfCnpj', 'angularMask']);

app.controller('FormController', function ($scope, $rootScope){

  $scope.ingressos = [
        { value: 1, name: 'Vestibular', id: 'vestibular', idEgoi: 'campoe_19_568_0' },
        { value: 2, name: 'ENEM', id: 'enem', idEgoi: 'campoe_19_568_1' },
        { value: 3, name: 'Transferência', id: 'transferencia', idEgoi: 'campoe_19_568_2' },
        { value: 4, name: 'Segunda Graduação', id: 'segunda-graduacao', idEgoi: 'campoe_19_568_3' }
    ];

    $scope.unidades = [
            {value : 1, name: 'Antonio Bezerra'},
            {value : 2, name: 'Messejana'},
            {value : 4, name: 'Montese'},
            {value : 3, name: 'Pecém'},
           ];

    $scope.SelectedItem = $scope.ingressos[0];

    $scope.changehappened = function(data){
    	$rootScope.$emit('nameselected', data);
    };

    $rootScope.$on('nameselected', function(evt, data){
    	$scope.selectedItem = data;
    });
});


function mandaForm(){
        var enviar = $('#e-goi').submit();
}
